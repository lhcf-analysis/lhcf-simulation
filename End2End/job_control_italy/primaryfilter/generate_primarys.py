#!/usr/bin/python3
'''
generate_primaries.py 
This script makes a loop of primaryfilter_v3 for all files stored in a given directory path.
It requires Python3.

Example: 
  python3 generate_primaries.py -i XXXXX -o XXXXX --type Op2022_Arm1_Center
'''
import os
import sys
import glob
import subprocess
import argparse

base_dire = "/opt/exp_software/lhcf/Simulation/LHCfSimulation/End2End/"

def Type2Tag(datatype):
    if 'Arm1_Center' in datatype:
        tag = 'a1c'
    elif 'Arm1_5mmHigh' in datatype:
        tag = 'a1h'
    elif 'Arm2_Center' in datatype:
        tag = 'a2c'
    elif 'Arm2_5mmHigh' in datatype:
        tag = 'a2h'
    return tag

if __name__ == '__main__':

    choices_type = ['Op2022_Arm1_Center', 'Op2022_Arm1_5mmHigh', 'Op2022_Arm2_Center', 'Op2022_Arm2_5mmHigh',
                    'Op2015_Arm1_Center', 'Op2015_Arm1_5mmHigh', 'Op2015_Arm2_Center', 'Op2015_Arm2_5mmHigh',]
    parser = argparse.ArgumentParser(prog='generate_primary',
                                     description='Generate primary files from DoubleArm output files')
    parser.add_argument('-i', '--input_dir', required=True,
                        default='', help='Directory path of input files')
    parser.add_argument('-o', '--output_dir', default='./tmp_primary',
                        help='Directory path of output files')
    parser.add_argument('--output_format', default='',
                        help='Output file format. if not given, automatically set using the input filename.')
    parser.add_argument('--type', required=True, choices=choices_type)
    args = parser.parse_args()

    input_path = args.input_dir   # End2End Output
    output_path = args.output_dir
    arg_output_format = args.output_format
    datatype = args.type

    exec_path = base_dire+'/primaryfilter/bin/primaryfilter_v3'
    if not os.path.isfile(exec_path):
	    print(exec_path+"does not exist: make another attempt")
	    exec_path = os.path.dirname(__file__)+'/../../primaryfilter/bin/primaryfilter_v3'
    if not os.path.isfile(exec_path):
	    print(exec_path+"does not exist: now it is time to exit")
	    sys.exit()

    # Get input file list
    if os.path.isdir(input_path):
        input_path += '/*' 
    filelist = glob.glob(input_path)

    # Create output directory
    os.makedirs(output_path, exist_ok=True)

    # Loop for input files 
    for file in filelist:

        check_gz = False
        # if the file is zipped, unzip it firstly
        if '.gz' in file:
            ret = subprocess.run(f'gunzip -f {file}', shell=True)
            file = file[:-3]   # file name without '.gz'
            check_gz = True

        # Set output filename (format) if not given
        output_format = ''
        if arg_output_format == '':
            tag = Type2Tag(datatype)
            output_format = os.path.basename(file)
            output_format = output_format.replace('darm_', f'e2eprimary_{tag}_')
            output_format = output_format.replace('.out', '_%03d.txt')
            output_format = output_path + '/' + output_format
        else :
            output_format = arg_output_format
            
        # convert to primary files
        command = f'{exec_path} -i {file} -o {output_format} '
        command += '--' + datatype
        command += ' --fileseparation 1000 --mhseparation --selection_anyhit -v 0'

        print(command)
        ret = subprocess.run(command, shell=True)

        # zip the file again
        if check_gz:
            ret = subprocess.run(f'gzip -f {file}', shell=True)
