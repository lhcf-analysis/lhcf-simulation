      program main
      implicit none

      integer i, j, k
      real(8):: x, y, eff
     
      eff = 0.
      
      do i=1, 32
         do j=1, 32
            x = i*0.1
            y = j*0.1
            call plate_eff(4,16, x, y, eff)
            write(*,'(2f4.1,f6.2)') x,y, eff
!            write(*,*) x,y, eff
         end do
      end do

      end program main
