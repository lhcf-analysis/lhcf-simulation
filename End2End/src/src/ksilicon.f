c     Modified by Alessio (December 2014)
c     - swapped X and Y views
c     - strip inversion (line 98) only for Y views
c     - !!! energy unit now is GeV !!! (removed 1000. factor in ksiliconr)

      subroutine ksiliconi(il)
      implicit none

      integer, save :: nlayer    ! input 
      integer ilayer, il
      real*8, save :: xsize(8) , ysize(8)       ! size 
      real*8, save :: xeffsize(8) , yeffsize(8) ! effective size  
      real*8, save :: xoffset(8) , yoffset(8)   ! offset of effective area
      real*8, save :: stripl(8)                 ! strip length [cm]
      integer, save :: nstrip(8)   ! number of strip
      integer, save :: xy(8)       ! directon of strip (x(0) or y(1))
      real*8, save :: sidE(8,384)
      integer i, j, itmp 
      real*8  dE , x , y , pp
      

      if(il .ne. 0 .and. il .ne. 8) then 
         write(0,*) 'ERROR Number of Si layers must be 0. or 8'
         stop 1000
      endif
      
      nlayer = il

      do ilayer = 1, nlayer
         itmp = MOD(ilayer, 2)
         if( itmp .eq. 0) then 
c           --- X view ---
            xy(ilayer) = 0;
            xsize(ilayer) = 6.356
            ysize(ilayer) = 6.396
            xeffsize(ilayer) = 6.144
            yeffsize(ilayer) = 6.200
            xoffset(ilayer)  = 0.102
            yoffset(ilayer)  = 0.098   
            stripl(ilayer) = 0.016
            nstrip(ilayer) = 384
         else
c           --- Y view ---
            xy(ilayer) = 1;
            xsize(ilayer) = 6.396
            ysize(ilayer) = 6.356
            xeffsize(ilayer) = 6.200
            yeffsize(ilayer) = 6.144
            xoffset(ilayer)  = 0.098
            yoffset(ilayer)  = 0.110 ! modified !!!
            stripl(ilayer) = 0.016
            nstrip(ilayer) = 384            
         endif
            
c         itmp = ilayer + 1
c         itmp = MOD(itmp, 4)
c         if( itmp .le. 1 ) then 
c            xy(ilayer) = 0 
c         else
c            xy(ilayer) = 1
c         endif

c         write(0,*) nstrip(ilayer)
      enddo
      
      return 
      
c     ************************
      entry ksiliconc(il)
c     ************************

      do ilayer = 1, nlayer
         do i = 1, nstrip(ilayer)
            sidE(ilayer , i) = 0.
         enddo
      enddo
      
      return 
      
c     ************************
      entry ksilicona(il, x , y ,dE)
c     ************************
      
c      write(*,'(a,i4,3f10.6)') 'come', il , x , y , dE
      x = x  - xoffset(il)
      y = y  - yoffset(il)  
      if(  x .gt. 0. .and. x .lt. xeffsize(il)  .and.
     *     y .gt. 0. .and. y .lt. yeffsize(il) ) then
         if( xy(il) .eq. 0) then
            pp = x
         elseif(  xy(il) .eq. 1) then 
            pp = y
         endif
         
         i = int(pp / stripl(il)) + 1
c         write(*,'(a,2i4,f10.6)') 'temp  ' , il , i , dE
         if( i .le. nstrip(il)) then
            if( xy(il) .eq. 1) then
               i = nstrip(il) - i + 1
            endif
            sidE(il, i) = sidE(il, i) + dE
c     write(*,'(a,2i4,f10.6)') 'detect' , il , i , sidE(il, i)
         endif
      endif
      
      return 

c     ************************
      entry ksiliconr()
c     ************************
      
      do ilayer = 1, nlayer 
         do i = 1 , nstrip(ilayer)
            if(sidE(ilayer, i) .gt. 0.) then
               write(*,'(a,i2,i4,a,g12.5)')  ' si ', ilayer , i ,
     *              ' ', sidE(ilayer,i)
            endif
         enddo
      enddo 
      end
