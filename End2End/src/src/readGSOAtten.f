 !!! #define TESTTIME
 
      real(8):: attX20(4,20,21) 
      real(8):: attY20(4,20,21) 
      real(8):: attX40(4,40,41) 
      real(8):: attY40(4,40,41) 

      integer:: ii, jj, kk
      integer,parameter:: io=10
      character(6):: ID
      real(8):: v
      logical,save::  firsttime = .true.
      real(8),parameter:: big=1.d100


      if( firsttime ) then
!!!  #ifdef TESTTIME; next needed only at TESTTIME but
!!!  may be kept; time loss  is negligile
         attX20(:, :, :) = big
         attY20(:, :, :) = big
         attX40(:, :, :) = big
         attY40(:, :, :) = big
!!!  #endif

         open(io, file="src/ForGSOAtten.dat")

         do 
            read(io, *,end=100) ID, ii, jj, kk, v
     
            select case( trim(ID) ) 
              case( "attX20" )
                 attX20(ii, jj, kk) = v
              case( "attY20" )
                 attY20(ii, jj, kk) = v
              case( "attX40" )
                 attX40(ii, jj, kk) = v
              case( "attY40" )          
                 attY40(ii, jj, kk) = v
              case default
                 write(0,*) ID, ' invalid'
                 stop
              end select
           enddo
 100       continue
           close(io)
!!! #ifdef TESTTIME
           if( any( attX20 == big ) ) then
              write(0, *) 'some attX20 not filled '
           endif
           if( any( attY20 == big ) ) then
              write(0, *) 'some attY20 not filled '
           endif
           if( any( attX40 == big ) ) then
              write(0, *) 'some attX40 not filled '
           endif
           if( any( attY40 == big ) ) then
              write(0, *) 'some attY40 not filled '
           endif
!! #endif
           firsttime = .false.
        endif
!!1        end program
