c     
c            (Not work under Absoft fortran 90) 
c      Usage:  ktoweri:       instanciate one tower
c              ktowerc:       clear dE counting area
c              ktowercmap:    clear dEmap 
c              ktowerascin:   add dE to scin
c              ktoweramap:    add dE form map
c              ktowerascifix: add dE to scifix
c              ktowerascifiy: add dE to scifiy
c              ktowerpmap:    print results
c              ktowerpscifi
c              ktowerpscin

c     Modified by Alessio (December 2014)
c     - added ktowerascin_truth subroutine
c     - modified "scin" to "gsoplate" in output file
c     - !!! energy unit now is GeV for gsoplate and scin !!! (removed spgsoplate and spscin)
c     - added parameter nfdTowers

      subroutine ktoweri(tw, nlin, 
     *      xsizein, ysizein, scifilin,name, scinpd)
      implicit none
c         instanciate a tower
c     ******************
#include "ZepPos.h"      
      include "src/Z90.f"
      type(aTower) tw
c     ====================      
      record /epPos/ posw, posl

      integer nlin     ! input. request nlayer tower
      real*8 xsizein     ! input. xsize of the tower
      real*8 ysizein     ! input. ysize of the tower
      integer scifilin ! input. no. of  sifilayers
      integer scinpd   ! input ON(1)/OFF(0) position dependance of scinti
      real*8 x, y, dE
      integer nev, world
      character*3 name
      integer nfdTowers
      parameter (nfdTowers = 2)
c
      integer i, j, k
      integer towerc
      data towerc /0/
      save towerc
      
      towerc = towerc + 1 
c//////////
c      write(*,*) ' instance for tw=', towerc, 
c     *  ' nlin,  xsizein, ysizein, scifilin=',
c     *  nlin,  xsizein, ysizein, scifilin
c///////
      tw%num = towerc
      tw%scifilayers = scifilin
      tw%xsize = xsizein
      tw%ysize = ysizein
      tw%nxscifi = int(xsizein/scifiw +1.d-5)
      tw%nyscifi = int(ysizein/scifiw +1.d-5)
      tw%nscin = nlin
      tw%nx = int(xsizein/ms+1.d-5)
      tw%ny = int(ysizein/ms+1.d-5)
      tw%name = name
      tw%scinpd = scinpd
      allocate( tw%scifixdE(tw%nxscifi,tw%scifilayers) )
      allocate( tw%scifiydE(tw%nyscifi,tw%scifilayers) )
      allocate( tw%scifixdE_att(tw%nxscifi,tw%scifilayers) )
      allocate( tw%scifiydE_att(tw%nyscifi,tw%scifilayers) )
      allocate( tw%scifixdE_truth(tw%nxscifi,tw%scifilayers) )
      allocate( tw%scifiydE_truth(tw%nyscifi,tw%scifilayers) )
      allocate( tw%scifixpos(tw%nxscifi) )
      allocate( tw%scifiypos(tw%nyscifi) )
      allocate( tw%scindE(tw%nscin) )
      allocate( tw%scindE_truth(tw%nscin) )
      allocate( tw%dEmap(tw%nx, tw%ny, tw%nscin))
      allocate( tw%mapxpos(tw%nx) )
      allocate( tw%mapypos(tw%ny) )

c/////////
c      write(*,*) ' tw%num ',tw%num 
c      write(*,*) ' tw%scifilayers',tw%scifilayers
c      write(*,*) ' tw%xsize', tw%xsize
c      write(*,*) ' tw%ysize', tw%ysize
c      write(*,*) ' tw%nxscifi ',tw%nxscifi
c      write(*,*) ' tw%nyscifi ',tw%nyscifi
c      write(*,*) ' tw%nscin ',tw%nscin
c      write(*,*) ' tw%nx ', tw%nx
c      write(*,*) ' tw%ny ', tw%ny
c       write(*,'(a,a)') ' tw%name ', tw%name
c///////////
      return
c     ***************
      entry ktowerscifixy(tw)
c     ****************
c        compute scifi x and y pos.
      if(tw%scifilayers .eq. 0) then
         return
      endif

      do j = 1, tw%nxscifi
         posl.x = (j-1)*scifiw + scifiw/2
         posl.y = 0.
         posl.z = 0.
         call epl2w(tw%fcn, posl, posw)
         tw%scifixpos(j) = posw.x
      enddo
      do j = 1, tw%nyscifi
         posl.x = 0.
         posl.y = (j-1)*scifiw + scifiw/2
         posl.z = 0.
         call epl2w(tw%fcn, posl, posw)
         tw%scifiypos(j) = posw.y
      enddo
      return
c     ***************
      entry ktowermapxy(tw)
c     ****************
c        compute map x and y pos.
      do j = 1, tw%nx
         posl.x = (j-1)*ms + ms/2
         posl.y = 0.
         posl.z = 0.
         call epl2w(tw%fcn, posl, posw)
         tw%mapxpos(j) = posw.x
      enddo
      do j = 1, tw%ny
         posl.x = 0.
         posl.y = (j-1)*ms + ms/2
         posl.z = 0.
         call epl2w(tw%fcn, posl, posw)
         tw%mapypos(j) = posw.y
      enddo
      return
      
c    ************************
      entry ktowerc(tw)
c    ************************
c//////
c      write(*,*) ' clear tower#=',tw%num
c      write(*,*) ' nscin=',tw%nscin, ' nx,ny=',
c     *    tw%nx,  tw%ny
c      write(*,*) ' scifilayers=', tw%scifilayers
c      write(*,*) ' tw%nx/yscifi=',tw%nxscifi,tw%nyscifi
c/////////
      do i = 1, tw%nscin
         tw%scindE(i) = 0.
         tw%scindE_truth(i) = 0.
      enddo
      do j = 1, tw%scifilayers
         do i = 1, tw%nxscifi
            tw%scifixdE(i,j) = 0.
            tw%scifixdE_att(i,j) = 0.
            tw%scifixdE_truth(i,j) = 0.
         enddo
         do i = 1, tw%nyscifi
            tw%scifiydE(i,j) = 0.
            tw%scifiydE_att(i,j) = 0.
            tw%scifiydE_truth(i,j) = 0.
         enddo
      enddo
      return
c    ************************
      entry ktowercmap(tw)
c    ************************
      do k = 1, tw%nscin
         do j = 1, tw%ny
            do i = 1, tw%nx
               tw%dEmap(i, j, k) = 0.
            enddo
         enddo
      enddo
      return 
c    *************************
      entry ktowerascin( tw, nlin, dE )
c    *************************
      tw%scindE(nlin) =  tw%scindE(nlin) + dE
c///////
c      write(*,*) ' tw=',tw%num, ' dE=',dE, ' added'
c      write(*,*) ' at layer=',nlin
c///////
      return
c     *************************
      entry ktowerascin_truth( tw, nlin, dE )
c     *************************
      tw%scindE_truth(nlin) =  tw%scindE_truth(nlin) + dE
c///////
c     write(*,*) ' tw=',tw%num, ' dE=',dE, ' added'
c     write(*,*) ' at layer=',nlin
c///////
      return
c     *************************
      entry ktoweramap( tw, nlin, x, y,  dE )
c     *************************
c     x, y is local coordinate in the tower
      i = int(x/ms) + 1
      j = int(y/ms) + 1
      if(i .ge. 1 .and. i .le. tw%nx) then
         if(j .ge. 1 .and. j .le. tw%ny ) then
            tw%dEmap(i, j, nlin) = tw%dEmap(i, j, nlin) + dE
         endif
      endif
      return
c    *************************
      entry ktowerascifix( tw, nlin, x,  dE )
c    *************************
c       nlin is the nlin-th scifi layer (not scin layer)
c       x is local coordinate in the tower
      i = int(x/scifiw) + 1
      if(i .ge. 1 .and. i .le. tw%nxscifi) then
         tw%scifixdE( i, nlin ) = tw%scifixdE( i, nlin )+dE
      endif
      return
c    *************************
      entry ktowerascifiy( tw, nlin, y,  dE )
c    *************************
c       nlin is the nlin-th scifi layer (not scin layer)
c       y is local coordinate in the tower
      i = int(y/scifiw) + 1
      if(i .ge. 1 .and. i .le. tw%nyscifi) then
         tw%scifiydE( i, nlin ) = tw%scifiydE( i, nlin )+dE
      endif
      return
c    *************************
      entry ktowerascifix_att( tw, nlin, x,  dE )
c    *************************
c       nlin is the nlin-th scifi layer (not scin layer)
c       x is local coordinate in the tower
      i = int(x/scifiw) + 1
      if(i .ge. 1 .and. i .le. tw%nxscifi) then
         tw%scifixdE_att( i, nlin ) = tw%scifixdE_att( i, nlin )+dE
      endif
      return
c    *************************
      entry ktowerascifiy_att( tw, nlin, y,  dE )
c    *************************
c       nlin is the nlin-th scifi layer (not scin layer)
c       y is local coordinate in the tower
      i = int(y/scifiw) + 1
      if(i .ge. 1 .and. i .le. tw%nyscifi) then
         tw%scifiydE_att( i, nlin ) = tw%scifiydE_att( i, nlin )+dE
      endif
      return
c    *************************
      entry ktowerascifix_truth( tw, nlin, x,  dE )
c    *************************
c       nlin is the nlin-th scifi layer (not scin layer)
c       x is local coordinate in the tower
      i = int(x/scifiw) + 1
      if(i .ge. 1 .and. i .le. tw%nxscifi) then
         tw%scifixdE_truth( i, nlin ) = tw%scifixdE_truth( i, nlin )+dE
      endif
      return
c    *************************
      entry ktowerascifiy_truth( tw, nlin, y,  dE )
c    *************************
c       nlin is the nlin-th scifi layer (not scin layer)
c       y is local coordinate in the tower
      i = int(y/scifiw) + 1
      if(i .ge. 1 .and. i .le. tw%nyscifi) then
         tw%scifiydE_truth( i, nlin ) = tw%scifiydE_truth( i, nlin )+dE
      endif
      return

c     *************
      entry ktowerpscin( nev,  tw )
c     ************* 
c         
c     Write out scintillators edep in MIP
      do i = 1, tw%nscin
         if( tw%scindE(i) .gt. -0.01 .and. tw%num .le. nfdTowers ) then
            write( *, '(a,a,a, i3, i3,  g14.6)' )
     *           ' scin ', tw%name, ' ', tw%num, i, tw%scindE(i)
         elseif( tw%scindE_truth(i) .gt. 0.0
     *           .and. tw%num .gt. nfdTowers ) then
c     OUTPUT:'"scin" "small/large" "component number" "layer" "edep with light collection" "true edep"'
            write( *, '(a,a,a, i3, i3,  2g14.6)' )
     *           ' gsoplate ',tw%name,' ' , tw%num,   i,
     *           tw%scindE(i), tw%scindE_truth(i)
c     *           tw%scindE(i), tw%scindE_truth(i)
         endif
c     <<< For Upgrade
      enddo
      return

c     *************
      entry ktowerpscifi( nev, world,  tw )
c     ************* 
c     OUTPUT: for only Arm1
      do k = 1, tw%scifilayers
         do j = 1, tw%nxscifi
            if(tw%scifixdE(j, k) .gt. 0.) then
               if(world .eq. 0 )then
                  write(*,'(a,a,a, i3, i2, i3,  3g18.9)') 
     *                 ' gsobarx ',tw%name,' ' , tw%num,   k, j, 
     *                 tw%scifixdE(j, k),
     *                 tw%scifixdE_att(j, k),
     *                 tw%scifixdE_truth(j, k)
               else
                  write(*,'(a,a,a,i3, i2, f8.3, 3g18.9)') 
     *                 ' gsobarx ',tw%name,' ' , tw%num,   k, 
     *                 tw%scifixpos(j),
     *                 tw%scifixdE(j, k),
     *                 tw%scifixdE_att(j, k),
     *                 tw%scifixdE_truth(j, k)
               endif
            endif
         enddo
      enddo

      do k = 1, tw%scifilayers
         do j = 1, tw%nyscifi
            if(tw%scifiydE(j, k) .gt. 0.) then
               if(world .eq. 0 )then
                  write(*,'(a,a,a, i3, i2, i3,  3g18.9)') 
     *                 ' gsobary ',tw%name,' ' , tw%num,   k, j, 
     *                 tw%scifiydE(j, k),
     *                 tw%scifiydE_att(j, k),
     *                 tw%scifiydE_truth(j, k)
               else
                  write(*,'(a,a,a,i3, i2, f8.3, 3g18.9)') 
     *                 ' gsobary ',tw%name,' ' , tw%num,   k, 
     *                 tw%scifiypos(j),
     *                 tw%scifiydE(j, k),
     *                 tw%scifiydE_att(j, k), 
     *                 tw%scifiydE_truth(j, k)
               endif
            endif
         enddo
      enddo

      return
c     ***************
      entry ktowerpmap( nev, world, tw )
c     ************* 
      do k = 1, tw%nscin
         do j = 1, tw%ny 
            do i = 1, tw%nx
               if( tw%dEmap(i, j, k) .gt. 0) then
                  if(world .eq. 0) then
                     write(*,'(a,a,a,i3,i3,i4,i4,g14.3)') 
     *                    ' map ',tw%name,' ', tw%num, k, i, j,
     *                    tw%dEmap(i,j,k)
                  else
                     write(*,'(a,a,a, i3, i3,2f9.4,g14.3)') 
     *                    ' map ' ,tw%name,' ' , tw%num,  k,
     *                    tw%mapxpos(i), tw%mapypos(j),
     *                    tw%dEmap(i,j,k)
                  endif
               endif
            enddo
         enddo
      enddo
      end
