c       fortran 90 version
c
c     Modified by Alessio (December 2014)
c     - added parameter (spgsobar=0.7641e-3, spgsoplate=0.7687e-3) 
c     - removed spgsoplate and spscin: !!! now energy unit is GeV !!! 
c     - TODO: remove spgsobar (for Arm1 GSO bars)
c     Modified by Makino (10-Dec-2014)
c     - remove spgsobar

c      real*8 spscifi, spscin
c      real*8 spgsobar, spgsoplate
c      real*8 spgsobar
c          single peak value in GeV for 1mm SciFi and 3 mm SCIN
c     parameter (spscifi=0.142e-3, spscin=0.453e-3)
c      parameter (spscifi=1.0, spscin=1.0)
c          single peak value in GeV for 1mm GSObar and 1 mm GSO
c      parameter (spgsobar=0.744e-3, spgsoplate=0.744e-3)
c      parameter (spgsobar=0.7641e-3, spgsoplate=0.7687e-3)      
c      parameter (spgsobar=0.7641e-3)

      real*8  scifiw  ! scifi width 
      real*8  ms      ! mesh size for energy loss map
      parameter( scifiw=0.1d0, ms=0.10d0 )
c      tower spec. input
c     #   x(cm)    y(cm)   scinlayers  scifilayers
c     1    2.5      2.5        17        6
c     unused channel of MAPMT
      real*8 unusedXdE(4, 4)
      real*8 unusedYdE(4, 4)

      type aTower
         integer num   ! tower number
         integer fcn   ! fiducial comp. number by which x,y can be measured by
                       !  local coordiante.
         character*3  name ! towername
         integer scinpd  ! ON(1)/OFF(0) position dependence in scintilators
       
         real xsize, ysize
         record /epPos/ fpos

         integer nxscifi,  nyscifi, scifilayers
         real, allocatable ::  scifixdE(:,:)
         real, allocatable ::  scifiydE(:,:)
         real, allocatable ::  scifixdE_att(:,:)
         real, allocatable ::  scifiydE_att(:,:)
         real, allocatable ::  scifixdE_truth(:,:)
         real, allocatable ::  scifiydE_truth(:,:)
         real, allocatable ::  scifixpos(:)
         real, allocatable ::  scifiypos(:)

         integer nscin 
         real, allocatable ::  scindE(:)
         real, allocatable ::  scindE_truth(:)

         integer nx, ny 
         real, allocatable ::  dEMap(:,:,:)

         real, allocatable ::  mapxpos(:)
         real, allocatable ::  mapypos(:)
         
      end type
