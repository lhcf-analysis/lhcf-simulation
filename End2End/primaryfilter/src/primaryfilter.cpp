#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>
#include <string.h>
using namespace std;

int cut_beampipe(double x,double y);

int main(int argc,char **argv){
  int  detector=0;
  char inputfilename[256];
  bool beampipecut=false;
  bool tanwallcut =false;
  bool bkgcut=false;
  bool inverse=false;

  for(int i=0;i<argc;i++){
    if(strcmp(argv[i],"-d")==0){
      if(strcmp(argv[i+1],"arm1")==0 || strcmp(argv[i+1],"Arm1")==0 ){
	detector = 1;
      }
      else if(strcmp(argv[i+1],"arm2")==0 || strcmp(argv[i+1],"Arm2")==0){
	detector = 2;
      }
      else {
	cerr << "Incorrect value: -d " << argv[i+1] << endl;
	return -1;
      }
      i++;
    } 
    if(strcmp(argv[i],"-i")==0){
      strcpy(inputfilename,argv[++i]);
    }
    if(strcmp(argv[i],"-wBPCut")==0){
      beampipecut=true;
    }
    if(strcmp(argv[i],"-woBPCut")==0){
      beampipecut=false;
    }
    if(strcmp(argv[i],"-wTANWCut")==0){
      tanwallcut=true;
    } 
    if(strcmp(argv[i],"-wBKGCut")==0){
      bkgcut=true;
    } 
    if(strcmp(argv[i],"-inverse")==0){
      inverse = true;
    }

    if(argc==1 ||
       strcmp(argv[i],"-h")==0 || strcmp(argv[i],"--help")==0){
      cout << " primaryfilter *************************************************\n"
	   << " ./primaryfilter -d [arm1/arm2] -i [inputfile] [-wBPCut] \n"
	   << " Options)\n"
	   << " -d      : detector [arm1 or arm2]\n"
	   << " -i      : input file name (output of DoubleArm)\n"
	   << " -wBPCut : with ellipse beam-pipe cut\n "
	   << "         : Use this cut for outputs of DoubleArm without matter.\n"
	   << " -wTANWCut: with tan wall cut (abs(x)>4.8cm)\n"
	   << " -wBKGCut:  with Backgound Cut.\n"
	   << " -inverse:  inverse input for the detector.\n"
	   << endl;
      return 0;
    }
  }

  ifstream fin(inputfilename);
  if(!fin){
    cout << "Canot Open " << inputfilename << endl; 
    return 0;
  }
  
  char   a[500];
  int    ndata;
  int    ncdata; 
  int    event,tag;
  int    pcode,psubcode,pcharge;
  double ke,r,x,y,z;
  double t,wx,wy,wz;
  double ox,oy;
  const double arm1_shiftx = 0.094;
  const double arm1_shifty = 0.11;
  const double arm2_shiftx = -0.3529;
  const double arm2_shifty = -0.0579;

  cout << "# mul sub KE xyz dir  user disk49" << endl
       << "#---------------------------------" << endl;

  cout.setf(ios::right);
  while(fin.getline(a,500)){
    // *** delete header info ***
    if(strlen(a) > 5 && strlen(a) < 40){
      continue;
    }
    // *** identiry a line feed ***
    if(strlen(a) < 5){
      cout << endl;
      //if(ndata!=0 && ncdata==0){
      //cout << endl;
      //}
      ndata = 0;
      ncdata = 0;
      continue;
    }
    // *** get data ***
    sscanf(a,"%d%d%d%d%d%lf%lf%lf%lf%lf%lf%lf%lf%lf",
	   &event,&tag,&pcode,&psubcode,&pcharge,
	   &ke,&r,&x,&y,&z,&t,&wx,&wy,&wz);
    ndata++;
    ox = x;
    oy = y;

    // *** convert for each arm ***
    if(detector==1){
      if(inverse==false){
	if(z<0.) continue;
	z = 0.; 
      }
      else{
	if(z>0.) continue;
	x  = -1.*x;
	wx = -1.*wx;
	z = 0.;
	wz = -1.*wz;
      }
      
      x += arm1_shiftx;
      y += arm1_shifty;
    }
    else if(detector==2){ 
      if(inverse==false){
	if(z>0.) continue;
	x  = -1.*x;
	wx = -1.*wx;
	z = 0.;
	wz = -1.*wz;
      }
      else{
	if(z<0.) continue;
	z = 0.;
      }

      x += arm2_shiftx;
      y += arm2_shifty;
    }
    
    // *** cut ***
    ndata++;
    if(sqrt(x*x + y*y) > 10.){continue;}
    if(beampipecut==true && cut_beampipe(ox*10.,oy*10.)==0){continue;}
    if(tanwallcut==true && fabs(x)>4.6){continue;}
    //if(ke < 10.){continue;}
    if(bkgcut==true && tag<0){continue;}
    ncdata++;

    // *** out data *** 
    cout.unsetf(ios::fixed);
    cout << setw(2) << pcode << " "
	 << setw(2) << psubcode << " "
	 << setw(2) << pcharge << " "
	 << setw(10) << setprecision(6) << ke << " "
	 << fixed
	 << setw(8) << setprecision(4) << x << " "
	 << setw(8) << setprecision(4) << y << " "
	 << setw(8) << setprecision(4) << z << " "
	 << setw(10) << setprecision(7) << wx << " "
	 << setw(10) << setprecision(7) << wy << " "
	 << setw(10) << setprecision(7) << wz << " "
	 << setw(8) << tag << " " 
	 << endl;
    //cout << x << "    " << y << endl;

    // if(event>10) break;
  }
  
  fin.close();
  
  return 0;
}

int cut_beampipe(double x,double y){
  double r2;
  r2 = (x/106.2)*(x/106.2) + (y/43.97)*(y/43.97);
  if(r2>1.0) return 0;
  else       return 1;
}
