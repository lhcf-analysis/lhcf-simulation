#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <iomanip>

#include <unistd.h>
#include <fstream>
#include <sstream>
#include <string>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <algorithm>

#include <TROOT.h>
#include <TFile.h>
#include <TH2.h>
#include <TPave.h>
#include <TCanvas.h>

using namespace std;

string input = "";
string output = "";
int nmaxfile = 0;
int arm = 0;
double detoff = 0.;

bool compareFunction (string a, string b) {return a<b;}

int is_regular_file(const char *path)
{
	struct stat path_stat;
	stat(path, &path_stat);
	return S_ISREG(path_stat.st_mode);
}

int is_regular_dir(const char *path)
{
	struct stat path_stat;
	stat(path, &path_stat);
	return S_ISDIR(path_stat.st_mode);
}

int is_out_file(const char *str, const char *suffix=".out")
{
	if (!str || !suffix)
		return 0;
	size_t lenstr = strlen(str);
	size_t lensuffix = strlen(suffix);
	if (lensuffix >  lenstr)
		return 0;
	return !strncmp(str + lenstr - lensuffix, suffix, lensuffix);
}

int get_files_in_folder(string input, std::vector <std::string> *output)
{
	DIR *dir;
	struct dirent *ent;
	int ifile=0;
	if ((dir = opendir (input.c_str())) != NULL) {
		/* print all the files and directories within directory */
		while ((ent = readdir (dir)) != NULL) {
			if (!strncmp(ent->d_name, ".", strlen(ent->d_name)) ||
					!strncmp(ent->d_name, "..", strlen(ent->d_name))) {
				continue;
			}
			if ( !is_out_file(ent->d_name) ) {
				continue;
			}
			output->push_back(Form("%s/%s", input.c_str(), ent->d_name));
			++ifile;
		}
		closedir (dir);
		sort( output->begin(), output->end() );
	} else {
		/* could not open directory */
		perror ("No Directory");
		return EXIT_FAILURE;
	}

	cout << "File to be processed :" << endl;
	for(unsigned int isize=0; isize<output->size(); ++isize)
		cout << output->at(isize) << endl;

	return ifile;
}

void HandleInputPar(int argc, char **argv)
{
	stringstream usage;
	usage.clear();
	usage << endl;
	usage << "Usage:" << endl << endl;
	usage << "-i\t\t\tinput file or folder" << endl;
	usage << "-n\t\t\tmaximum number of file" << endl;
	usage << "-a\t\t\tdetector arm is 1 or 2" << endl;
	usage << "-d\t\t\ty detector offset" << endl;
	usage << "-o\t\t\toutput file" << endl;
	usage << "-h\t\t\thelp" << endl;

	int c;

	while ((c = getopt(argc, argv, "i:n:a:d:o:h")) != -1){
		switch (c) {
		case 'i':
			input = optarg;
			if (input=="") {
				cout << "Error: Please enter a valid input file or directory name. Exit...\n"; exit(EXIT_FAILURE);
			}
			break;
		case 'n':
			nmaxfile = atoi(optarg);
			if (nmaxfile<=0) {
				cout << "Error: Please enter a valid number of files. Exit...\n"; exit(EXIT_FAILURE);
			}
			break;
		case 'a':
			arm = atoi(optarg);
			if (arm!=1 && arm!=2) {
				cout << "Error: Please enter a valid arm number. Exit...\n"; exit(EXIT_FAILURE);
			}
			break;
		case 'd':
			detoff = atof(optarg);
			break;
		case 'o':
			output = optarg;
			if (output=="") {
				cout << "Error: Please enter a valid output file name. Exit...\n"; exit(EXIT_FAILURE);
			}
			break;
		case 'h':
			cout << usage.str().c_str() << endl;
			exit(0);
			break;
		default:
			cout << usage.str().c_str() << endl;
			exit(0);
			break;
		}
	}

	if(arm==0) {
		cout << "Please specify the arm number!\n"; exit(EXIT_FAILURE);
		cout << usage.str().c_str() << endl;
		exit(0);
	}
}

int main(int argc, char** argv)
{
	HandleInputPar(argc, argv);

	//Check input file
	std::vector <std::string> filename;
	int nuse_file=0;
	if(is_regular_file(input.c_str()))
		nuse_file=1;
	else {
		if(is_regular_dir(input.c_str())) {
			nuse_file = get_files_in_folder(input.c_str(), &filename);
			std::sort(filename.begin(),filename.end(),compareFunction);
		} else {
			cout << "Not such file or directory " << input.c_str() << " : Exit..." << endl;
			return -1;
		}
	}
	if(nmaxfile>0 && nuse_file>0 && nuse_file>nmaxfile)
		nuse_file = nmaxfile;

	//Make Output
	TFile *output_file = new TFile(output.c_str(), "RECREATE");
	output_file->cd();

	TH2D *hvertex = new TH2D("Vertex", "All particles; x [cm]; y [cm]", 500, -10, 10, 500, -10, 10);

	for(int ifile=0; ifile<nuse_file; ++ifile)
	{
		if(ifile%100==0 || ifile==nuse_file-1)
			cout << "Loading file " << (nuse_file==1? input : filename[ifile].c_str()) << endl;

		ifstream input_file(nuse_file==1? input : filename[ifile].c_str());

		string line;

		const int buf_size = 1024;
		char buf[buf_size];

		while (input_file)
		{
			input_file.getline(buf, buf_size);
			if (!strcmp(buf, "")) continue;  // Skip empty lines
			if (buf[0] == '#') continue;     // Skip comment lines

			int code, subcode, charge;
			double energy;
			double posx, posy, posz;
			double wx, wy, wz;
			double tag;

			sscanf(buf, "%d %d %d %lf %lf %lf %lf %lf %lf %lf %lf", //
					&code, &subcode, &charge, //
					&energy, //
					&posx, &posy, &posz, //
					&wx, &wy, &wz, //
					&tag);

			hvertex->Fill(posx, posy);
		}

		input_file.close();
	}

	TPave *st = NULL;
	TPave *lt = NULL;

	TCanvas *cc = new TCanvas("Hitmap", "", 600, 600);
	cc->cd();
	hvertex->Draw("COLZ");
	if(arm==2) {
		st = new TPave(+1.7, -1.25+detoff, -0.80, 1.25+detoff);
		lt = new TPave(-0.80-0.18, 1.25+0.18+detoff, -0.80-0.18-3.20, 1.25+0.18+3.20+detoff);
		st->SetFillStyle(0);
		st->SetLineWidth(2);
		st->SetLineStyle(1);
		lt->SetFillStyle(0);
		lt->SetLineWidth(2);
		lt->SetLineStyle(1);
		st->Draw("same");
		lt->Draw("same");
	} else {
		cout << "TODO: No detector contour yet implemented for Arm1!" << endl;
	}

	cc->Write();
	hvertex->Write();
	output_file->Close();

	return 0;
}
