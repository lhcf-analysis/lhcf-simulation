#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>
#include <string.h>
#include <vector>
#include <sstream>
#include <algorithm>
using namespace std;

class Particle {
public:
  int    pcode;
  int    psubcode;
  int    pcharge;
  double ke;
  double x;
  double y;
  double z;
  double wx;
  double wy;
  double wz;
  int    tag;
public:
  Particle(int a_pcode, int a_psubcode, int a_pcharge,
	   double a_ke, double a_x, double a_y, double a_z, 
	   double a_wx, double a_wy, double a_wz, int a_tag) :
    pcode(a_pcode), psubcode(a_psubcode), pcharge(a_pcharge), 
    ke(a_ke), x(a_x), y(a_y), z(a_z), wx(a_wx), wy(a_wy), wz(a_wz), tag(a_tag) {;}
  
  
  string Print(){
    ostringstream sout;
    sout.unsetf(ios::fixed);
    sout << setw(2) << pcode << " "
	 << setw(2) << psubcode << " "
	 << setw(2) << pcharge << " "
	 << setw(10) << setprecision(6) << ke << " "
	 << fixed
	 << setw(8) << setprecision(4) << x << " "
	 << setw(8) << setprecision(4) << y << " "
	 << setw(8) << setprecision(4) << z << " "
	 << setw(10) << setprecision(7) << wx << " "
	 << setw(10) << setprecision(7) << wy << " "
	 << setw(10) << setprecision(7) << wz << " "
	 << setw(10) << tag << " " 
	 << endl;
    return sout.str();
  }
  
  // For Sorting by Kinetic Energy 
  bool operator<(const Particle &another) const {
    return ke < another.ke;
  }
};

class ParticleList {
public:
  int fNevent;
  int fNparticle;
  int fNoutevents;
  vector<Particle> fParticleList;

public:
  ParticleList(): fNevent(0), fNoutevents(0) {
    Clear();
  }
    
  void Clear(){
    fNparticle = 0;
    fParticleList.clear();
  }
  void Add(int eventid, int a_pcode, int a_psubcode, int a_pcharge,
	   double a_ke, double a_x, double a_y, double a_z, 
	   double a_wx, double a_wy, double a_wz, int a_tag){
    
    int tag;
    if(a_tag < 10000){
      tag = a_tag + eventid*10000;  // combine event ID + parent ID
    }
    else{
      tag = 9999 + eventid*10000;
    }
    fParticleList.push_back(Particle(a_pcode, a_psubcode, a_pcharge,
				     a_ke, a_x, a_y, a_z, a_wx, a_wy, a_wz, tag));
    fNparticle++;
  }
  void Sort(){
    sort(fParticleList.rbegin(), fParticleList.rend());
  }
  void Print(bool option_mhseparation, ostream &sout){
    bool bcheck=false;
    // Increment the event ID 
    fNevent++;
    
    if(option_mhseparation && fParticleList.size()==0){
      return ;
    }
    
    for(int i=0;i<fNparticle;i++){
      bcheck=false;
      sout << fParticleList[i].Print();
      
      if(option_mhseparation && fParticleList[i].ke > 20){
	sout << endl;
	fNoutevents++;
	bcheck=true;
      }
    }
    // End of event
    if(!bcheck){
      sout << endl;
      fNoutevents++;
    }
    return ;
  }
};

// -------------------------------------------------
int cut_beampipe(double x,double y){
  double r2;
  r2 = (x/106.2)*(x/106.2) + (y/43.97)*(y/43.97);
  if(r2>1.0) return 0;
  else       return 1;
}

// Main --------------------------------------------
int main(int argc,char **argv){
  int  detector=0;
  string inputfilename;
  string outputfilename;
  bool beampipecut=false;
  bool tanwallcut =false;
  bool bkgcut=false;
  bool inverse=false;
  bool mhseparation=false;

  for(int i=0;i<argc;i++){
    if(strcmp(argv[i],"-d")==0){
      if(strcmp(argv[i+1],"arm1")==0 || strcmp(argv[i+1],"Arm1")==0 ){
	detector = 1;
      }
      else if(strcmp(argv[i+1],"arm2")==0 || strcmp(argv[i+1],"Arm2")==0){
	detector = 2;
      }
      else {
	cerr << "Incorrect value: -d " << argv[i+1] << endl;
	return -1;
      }
      i++;
    } 
    if(strcmp(argv[i],"-i")==0 || strcmp(argv[i],"--input")==0){
      inputfilename = argv[++i];
    }
    if(strcmp(argv[i],"-o")==0 || strcmp(argv[i],"--output")==0){
      outputfilename = argv[++i];
    }
    if(strcmp(argv[i],"-wBPCut")==0){
      beampipecut=true;
    }
    if(strcmp(argv[i],"-woBPCut")==0){
      beampipecut=false;
    }
    if(strcmp(argv[i],"-wTANWCut")==0){
      tanwallcut=true;
    } 
    if(strcmp(argv[i],"-wBKGCut")==0){
      bkgcut=true;
    } 
    if(strcmp(argv[i],"-inverse")==0){
      inverse = true;
    }
    if(strcmp(argv[i],"-mhseparation")==0){
      mhseparation = true;
    }
    
    if(argc==1 ||
       strcmp(argv[i],"-h")==0 || strcmp(argv[i],"--help")==0){
      cout << " primaryfilter *************************************************\n"
	   << " ./primaryfilter -d [arm1/arm2] -i [inputfile] -o [outputfile] \n"
	   << " Options)\n"
	   << " -d      : detector [arm1 or arm2]\n"
	   << " -i      : input file name (output of DoubleArm)\n"
	   << " -o      : output file name (\"+primary\")\n"
	   << " -wBPCut : with ellipse beam-pipe cut\n "
	   << "         : Use this cut for outputs of DoubleArm without matter.\n"
	   << " -wTANWCut: with tan wall cut (abs(x)>4.8cm)\n"
	   << " -wBKGCut:  with Backgound Cut.\n"
	   << " -inverse:  inverse input for the detector.\n"
	   << " -mhseparation: generate event for individual incident particle.\n"
	   << endl;
      return 0;
    }
  }

  // Open Input/Output file
  ifstream fin(inputfilename.c_str());
  if(!fin){
    cout << "Error: Could not open " << inputfilename << endl; 
    return -1;
  }

  ofstream fout(outputfilename.c_str());
  
  
  char   a[500];
  int    ndata;
  int    ncdata; 
  int    event,tag;
  int    pcode,psubcode,pcharge;
  double ke,r,x,y,z;
  double t,wx,wy,wz;
  double ox,oy;
  const double arm1_shiftx = 0.094;
  const double arm1_shifty = 0.11;
  const double arm2_shiftx = -0.3529;
  const double arm2_shifty = -0.0579;

  ParticleList plist;
  plist.Clear();

  fout << "# mul sub KE xyz dir  user disk49" << endl
       << "#---------------------------------" << endl;

  fout.setf(ios::right);
  while(fin.getline(a,500)){
    // *** delete header info ***
    if(strlen(a) > 5 && strlen(a) < 40){
      continue;
    }
    // *** identiry a line feed ***
    if(strlen(a) < 5){
      // End of the event (one collision)
      // Sort by Kinetic Energy
      plist.Sort();
      // Print the result
      plist.Print(mhseparation, fout);

      plist.Clear();
      ndata = 0;
      ncdata = 0;
      continue;
    }
    // *** get data ***
    sscanf(a,"%d%d%d%d%d%lf%lf%lf%lf%lf%lf%lf%lf%lf",
	   &event,&tag,&pcode,&psubcode,&pcharge,
	   &ke,&r,&x,&y,&z,&t,&wx,&wy,&wz);
    ndata++;
    ox = x;
    oy = y;

    // *** convert for each arm ***
    if(detector==1){
      if(inverse==false){
	if(z<0.) continue;
	z = 0.; 
      }
      else{
	if(z>0.) continue;
	x  = -1.*x;
	wx = -1.*wx;
	z = 0.;
	wz = -1.*wz;
      }
      
      x += arm1_shiftx;
      y += arm1_shifty;
    }
    else if(detector==2){ 
      if(inverse==false){
	if(z>0.) continue;
	x  = -1.*x;
	wx = -1.*wx;
	z = 0.;
	wz = -1.*wz;
      }
      else{
	if(z<0.) continue;
	z = 0.;
      }

      x += arm2_shiftx;
      y += arm2_shifty;
    }
    
    // *** cut ***
    ndata++;
    if(sqrt(x*x + y*y) > 10.){continue;}
    if(beampipecut==true && cut_beampipe(ox*10.,oy*10.)==0){continue;}
    if(tanwallcut==true && fabs(x)>4.6){continue;}
    //if(ke < 10.){continue;}
    if(bkgcut==true && tag<0){continue;}
    ncdata++;
  
    // Fill to the list 
    plist.Add(event, pcode, psubcode, pcharge, ke, x, y, z, wx, wy, wz, tag);

    // if(event>10) break;
  }
  
  fin.close();
  fout.close();

  // Print the infromation 
  cout << "Input file:  " << inputfilename << endl
       << "Output file: " << outputfilename << endl
       << "Number_of_collisions:    " << plist.fNevent << endl
       << "Number_of_output_events: " << plist.fNoutevents << endl;
   
  return 0;
}


