from math import *

nev = 100
crossingangle = 145.E-6
distance = 13980.

for i in range (nev):
    pcode = 1
    psubcode = 0
    pcharge = 0
    x = 0.0
    y = distance*tan(crossingangle)
    z = 13980.050
    r = sqrt(x*x + y*y)
    ke = 500.
    wx = 0.
    wy = sin(crossingangle)
    wz = cos(crossingangle)
    
    v = '{:6d} '.format(i+1)
    v += '{:10d} '.format(i*100+1)
    v += '{:4d} {:2d} {:2d}'.format(pcode, psubcode, pcharge)
    v += '{:10.4f}  '.format(ke)
    v += '{:8.5f} {:8.5f} {:8.5f} {:12.5f} '.format(r,x,y,z)
    v += ' 0.000 '
    v += '{:12.8f} {:12.8f} {:12.8f}'.format(wx, wy, wz)

    print(v)
    print("")