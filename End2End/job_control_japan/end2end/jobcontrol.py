'''
Submit controller of End2End jobs to Nagoya or ICRR cluster.
This works with both python 2.7 and python 3

The random seed is automatically set using numbers in the input filename.

please use '_' as a word separation. 

'''

import subprocess
import time
import sys
import os
import re
import glob
from datetime import datetime, timedelta
from logging import getLogger, StreamHandler, Formatter, FileHandler, DEBUG
from socket import gethostname

if sys.version_info.major == 3:
    # Python 3
    unicode = str

class JobController:
    def __init__(self) -> None:
        self.user = ''
        self.logger = getLogger(__name__)
        self.hostname = gethostname()  # icrhome10 or lhcfs2.stelab.nagoya-u.ac.jp
        self.dir_run = './run/'
        self.dir_submitlog = './submitlog/'

        # Cluster control command
        if 'icrhome' in self.hostname:
            self.jobstat = 'pjstat'
            self.jobsub = 'pjsub'
        elif 'lhcfs2' in self.hostname:
            self.jobstat = 'qstat'
            self.jobsub = 'qsub'
        else:
            print('Unkown cluster system.')
            exit()

    def set_user(self, username):
        self.user = username

    def make_directories(self):
        if not os.path.exists(self.dir_run):
            os.makedirs(self.dir_run)
        if not os.path.exists(self.dir_submitlog):
            os.makedirs(self.dir_submitlog)

    def setup_logger(self, logfile='submitlog/submitlog.txt'):
        self.logger = getLogger(__name__)
        self.logger.setLevel(DEBUG)

        sh = StreamHandler()
        sh.setLevel(DEBUG)
        formatter = Formatter('%(asctime)s: %(message)s')
        sh.setFormatter(formatter)
        self.logger.addHandler(sh)

        fh = FileHandler(logfile)  # fh = file handler
        fh.setLevel(DEBUG)
        fh_formatter = Formatter('%(asctime)s : %(message)s')
        fh.setFormatter(fh_formatter)
        self.logger.addHandler(fh)
        return self.logger

    def njob(self):
        ret = subprocess.check_output(
            '{} | grep {} | grep -v " ERR " | wc'.format(self.jobstat, self.user), shell=True)
        vals = ret.split()
        n = int(vals[0])
        return n

    def waitjob(self, limit=500):
        while True:
            if self.njob() <= limit:
                break
            time.sleep(10)
        return

    def submit_job(self, orgfile, model, pfile, pfile_path, run_tag='', ofile='', seed=-1):
        # Set the ofile, tag, and seed automatically
        pfile_base = os.path.splitext(os.path.basename(pfile))[0]
        if ofile == '':
            ofile = pfile_base.replace('e2eprimary_', 'e2e_')
            ofile += '.out'
        if run_tag == '':
            run_tag = pfile_base
        if seed < 0:
            tmp = pfile_base.replace(model, '')
            list = re.split('\_|\.|run', tmp)
            runs = [int(i) for i in list if unicode(i).isdecimal()]
            if len(runs) >= 2:
                seed = runs[-1] + runs[-2]*10000
            elif len(runs) == 1:
                seed = runs[0]
            else:
                seed = -1

        f = open(orgfile, 'r')
        data = f.read()
        f.close()

        # Replace the parameters
        # Run Number
        # data = data.replace('$1', '{}'.format(run))
        # Model name
        data = data.replace('EPOSLHC', model)
        data = data.replace('${__INPUT_DIR__}', pfile_path)
        data = data.replace('${__INPUT_FILE__}', pfile)
        data = data.replace('${__OUTPUT_FILE__}', ofile)
        data = data.replace('${__FILE_TAG__}', run_tag)
        data = data.replace('${__SEED__}', '{}'.format(seed))

        filename = './{}/{}_{}.sh'.format(self.dir_run, model, run_tag)
        f = open(filename, mode='w')
        f.write(data)
        f.close()

        ret = subprocess.call('{} {} '.format(
            self.jobsub, filename), shell=True)
        self.logger.info('submit {} ({})'.format(filename, orgfile))
        return

    def send_mail(self, n_submitted, n_total, n_days, start_time):
        # Send a daily report by email

        f = open("mail_{}.txt".format(self.user), 'r')
        data = f.read()
        f.close()

        data = data.replace('__SUBMITTED__', '{}'.format(n_submitted))
        data = data.replace('__TOTAL__', '{}'.format(n_total))
        data = data.replace('__NDAYS__', '{}'.format(n_days))
        data = data.replace('__START_DATE__', start_time.strftime('%Y/%m/%d'))

        filename = "./{}/mail_{}.txt".format(self.dir_submitlog, self.user)
        f = open(filename, 'w')
        f.write(data)
        f.close()

        ret = subprocess.call(
            'cat {} | sendmail -i -t'.format(filename), shell=True)
        return

    def loop_jobs(self, model, pfile_path, limit=500, orgfile='run_arm1_center.sh'):

        # absolute path
        pfile_path = os.path.abspath(pfile_path)

        # list of primary files
        tmp = pfile_path
        if os.path.isdir(pfile_path):
            tmp += '/*'
        else:
            print('Error: Provide a directory path of primary files.')
            return

        filelist = glob.glob(tmp)
        filelist.sort()

        # loop of files
        n_submitted = 0
        n_total = len(filelist)

        start_time = datetime.now()
        pre_time = start_time
        for pfile in filelist:
            print('pfile1 = '+pfile)
            self.waitjob(limit)
            self.submit_job(orgfile, model, pfile, pfile_path)
            n_submitted += 1

            current_time = datetime.now()
            if current_time > pre_time + timedelta(days=1):
                self.send_mail(n_submitted, n_total,
                               (current_time - start_time).days, start_time)
                pre_time = datetime.now()

