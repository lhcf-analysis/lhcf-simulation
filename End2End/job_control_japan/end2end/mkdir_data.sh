#!/bin/bash 

# Please execute this script before you submit jobs 
# because log ouput director must be present before your job submission.

DATADIR="/disk/lhcf/user/simulation_Run3/Data/End2End/v1/"

mkdir -p $DATADIR/Arm1_Center/EPOSLHC/log/
mkdir -p $DATADIR/Arm1_Center/QGSJET2_04/log/
mkdir -p $DATADIR/Arm1_5mmHigh/EPOSLHC/log/
mkdir -p $DATADIR/Arm1_5mmHigh/QGSJET2_04/log/
#mkdir -p $DATADIR/SIBYLL2.3d/log/
#mkdir -p $DATADIR/DPMJET3_2019/log/

DATADIR="/disk/lhcf/user/simulation_Run3/Data/End2End/v1_wPipe/"

mkdir -p $DATADIR/Arm1_Center/EPOSLHC/log/
mkdir -p $DATADIR/Arm1_5mmHigh/EPOSLHC/log/
#mkdir -p $DATADIR/hepmc/QGSJET2_04/log/
#mkdir -p $DATADIR/hepmc/SIBYLL2.3d/log/
#mkdir -p $DATADIR/hepmc/DPMJET3_2019/log/
