from jobcontrol import JobController

# main
if __name__ == '__main__':

    con = JobController() 
    con.set_user('menjo')
    con.make_directories()
    con.setup_logger(logfile="./submitlog/log.txt")

    #con.loop_jobs(model='EPOSLHC', pfile_path='./tmp_primary/')
    con.loop_jobs(model='QGSJET2_04', pfile_path='./tmp_primary/')

