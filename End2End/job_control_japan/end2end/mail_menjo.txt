From:<menjo@icrr.u-tokyo.ac.jp>
To:<menjo@isee.nagoya-u.ac.jp>
Subject: ICRR CRMC Jobs

End2End Simulation status

  - __SUBMITTED__ jobs of total __TOTAL__ have been submitted.   
  - __NDAYS__ days passed since __START_DATE__ 

Please confirm your job status.
$pjstat --summary

