#!/bin/bash 

# Please execute this script before you submit jobs 
# because log ouput director must be present before your job submission.

DATADIR="/disk/lhcf/user/simulation_Run3/Data/crmc_1.8.0/"

mkdir -p $DATADIR/hepmc2gencol/
mkdir -p $DATADIR/hepmc/EPOSLHC/log/
mkdir -p $DATADIR/hepmc/QGSJET2_04/log/
mkdir -p $DATADIR/hepmc/SIBYLL2.3d/log/
mkdir -p $DATADIR/hepmc/DPMJET3_2019/log/


mkdir -p $DATADIR/hepmc_forwPipe/EPOSLHC/log/

# For QGSJET3

DATADIR2="/disk/lhcf/user/simulation_Run3/Data/crmc-v20241127/"
mkdir -p $DATADIR2/hepmc/QGSJET3_01/log/
mkdir -p $DATADIR2/hepmc2gencol/QGSJET3_01/
ln -s $DATADIR2/hepmc/QGSJET3_01 $DATADIR/hepmc/
ln -s $DATADIR2/hepmc2gencol/QGSJET3_01 $DATADIR/hepmc2gencol/
