#ifndef CONSTANTS_H_
#define CONSTANTS_H_

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <TH1D.h>
#include <TMath.h>

using namespace std;

/* Constants */

//Arm parameters
const int narm	      = 2;
const int ntower      = 2;
const int nxyposdet   = 2;
const int nxyz	      = 3;
const double iptotan  = 139.80; //m
const double iptolhcf = 141.050; //m

//p-p sqrt(s)=13 TeV
const double beam_energy = 6.8; //TeV  sqrt(s)
const double beam_mass = 938.2720813*TMath::Power(10., -6.); //TeV
const double beam_momentum = TMath::Sqrt(TMath::Power(beam_energy, 2.)-TMath::Power(beam_mass, 2.)); //TeV
const double beamcenterx = -3.272; //mm  ***Fill 3855***
const double beamcentery = -2.689; //mm  ***Fill 3855***

//Position selection criteria for standard analysis
const int nfvbin = 7;
const double r_interval[nfvbin][2] = //mm
  {
    { 0.,  5.},
    { 5., 10.},
    {10., 15.},
    {28., 35.},
    {35., 42.},
    {42., 49.},
    {49., 56.}
  };

//Event selection criteria in MC
const double Enethre_CRMC = 200.; //GeV (on spectra after unfolding)
const int photonrange[2]={1, 2}; // code relative to photon, electron
const int hadronrange[2]={4, 30}; // code relative to hadrons (photon, electron and muon removed)

#endif /*CONSTANTS_H_*/
