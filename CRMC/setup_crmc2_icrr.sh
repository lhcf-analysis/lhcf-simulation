# For CRMC MC generation

# module load python/3.8.3
# module load cmake/3.17.3
module load gcc/8.3.1

export CC=/opt/rh/devtoolset-8/root/usr/bin/gcc
export CXX=/opt/rh/devtoolset-8/root/usr/bin/g++

export BOOST_ROOT=/disk/lhcf/user/simulation_Run3/local2/
export HEPMC_ROOT=/disk/lhcf/user/simulation_Run3/local2/
#export HEPMC3_ROOT=/disk/lhcf/user/simulation_Run3/loca2l/
#export PYTHON3_ROOT=/disk/lhcf/user/simulation_Run3/local2/
#export LD_LIBRARY_PATH=$BOOST_ROOT/lib:$HEPMC3_ROOT/lib64:$LD_LIBRARY_PATH
#export PATH=/disk/lhcf/user/simulation_Run3/local2/bin/:$PYTHON3_ROOT/bin:$PATH
export LD_LIBRARY_PATH=$BOOST_ROOT/lib:$LD_LIBRARY_PATH
export PATH=/disk/lhcf/user/simulation_Run3/local2/bin/:$PATH

export LHCF_SIM_BASE=/disk/lhcf/user/simulation_Run3/lhcf-simulation/
export CRMC_DIR=/disk/lhcf/user/simulation_Run3/local2/crmc-v20241127/

#source /disk/lhcf/user/simulation_Run3/local2/root6/bin/thisroot.sh


