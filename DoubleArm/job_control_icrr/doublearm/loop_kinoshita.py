from jobcontrol import JobController

# main
if __name__ == '__main__':

    con = JobController() 
    con.set_user('kokinosh')
    con.make_directories()
    con.setup_logger(logfile="./submitlog/log.txt")

    # DoubleArm Mass Production for >3 hit study 
    con.loop_jobs(model='EPOSLHC', srun=30001, erun=60000, limit=500, orgfile='run_crmc_darm.sh')
