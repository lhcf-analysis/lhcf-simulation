#!/usr/local/python-3.8/bin/python
import subprocess

completedProcess = subprocess.run(['pjstat'], check=True, shell=True, stdout=subprocess.PIPE)
lines = completedProcess.stdout.decode('utf-8').split('\n')
#print(lines)

for line in lines:
    if len(line) < 10 :
        continue
    pid = line.split()[0]
    if pid == 'JOB_ID':
        continue
    command = 'pjdel {}'.format(pid)
    ret = subprocess.call(command.split()) 
    if ret==0 :
        print(command)
