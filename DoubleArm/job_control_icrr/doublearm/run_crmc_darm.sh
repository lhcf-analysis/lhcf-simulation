#!/bin/bash
#------ pjsub option --------#
#PJM -L "rscgrp=A"
#PJM -L "vnode=1"
#PJM -L "vnode-core=1"
#PJM -j
#PJM -o "/disk/lhcf/user/simulation_Run3/Data/DoubleArm/v1_mass/EPOSLHC/log/log_run$1.out"
#------- Program execution -------#
# History
#    2022 Nov. 11th : copied from the original file in /disk/lhcf/user/menjo/RHICf/Workspace/MCtrue/run/run.sh and removed script related to conversion and transports. 

#
#  This script is for mass production of doublearm results with CRMC event generation.
#  The generated CRMC output and intermidiate files are removed to save the disk space 
#  when doublearm is completed. 

source /disk/lhcf/user/simulation_Run3/lhcf-simulation/CRMC/setup_crmc_icrr.sh
source /disk/lhcf/user/simulation_Run3/lhcf-simulation/DoubleArm/setup_epics_icrr.sh

RUN=$1
RUNTMP=`printf %06d $RUN`

MODEL="EPOSLHC"

DATADIR="/disk/lhcf/user/simulation_Run3/Data/"
WORKDIR="${DATADIR}/DoubleArm/tmp/${MODEL}_${RUN}/"
OUTPUT_DIR="${DATADIR}/DoubleArm/v1_mass/${MODEL}/"
OUTPUT_FILENAME="darm_${MODEL}_${RUNTMP}.out"

# Setup 
mkdir -p ${WORKDIR}
mkdir -p ${OUTPUT_DIR}/log
mkdir -p ${OUTPUT_DIR}/old
cd ${WORKDIR}

date

# Check presence of old file
if [ -f "$OUTPUT_DIR/$OUTPUT_FILENAME.gz" ];then
    mv $OUTPUT_DIR/$OUTPUT_FILENAME.gz $OUTPUT_DIR/old/
    echo "mv $OUTPUT_DIR/$OUTPUT_FILENAME.gz $OUTPUT_DIR/old/"
    echo "OLD FILE MOVED TO $OUTPUT_DIR/old"
elif [ -f "$OUTPUT_DIR/$OUTPUT_FILENAME" ];then
    mv $OUTPUT_DIR/$OUTPUT_FILENAME $OUTOUT_DIR/old
    echo "OLD FILE MOVED TO $OUTPUT_DIR/old"
fi

######## CRMC ########

MODELOP="-m0"

OUTDIR_CRMC="${WORKDIR}"
OUTDIR_CONV="${WORKDIR}"

# crmc (CRMC_DIR is defined in setup_crmc_icrr.sh)
CRMC_BIN="${CRMC_DIR}/bin/crmc"
CRMC_PARAM="${LHCF_SIM_BASE}/CRMC/src/crmc/crmc_lhcf_icrr.param"
CRMC_OUTFILE="crmc_${MODEL}_${RUNTMP}.hepmc.gz"
CRMC_OUT="${WORKDIR}/${CRMC_OUTFILE}"
#cd ${CRMC_DIR}
${CRMC_BIN} -o hepmcgz -i2212 -I2212 -p6800 -P-6800 -n100000 -s ${RUN} -c ${CRMC_PARAM} ${MODELOP} -f${CRMC_OUT} 

# move the output files
#cp $CRMC_OUT $OUTDIR_CRMC

# conversion from hepmc.gz to gencol format 
CONV_DIR="${LHCF_SIM_BASE}/CRMC/src/conversion/"
CONV_BIN="${CONV_DIR}/bin/HepMCtoGencol"
CONV_OUTFILE="gen_${MODEL}_${RUNTMP}.out"
CONV_OUT="$OUTDIR_CONV/$CONV_OUTFILE"
LOG_FILE="${OUTDIR_CONV}/log_hepmc2gencol_${RUNTMP}.log"
QGSJET2_FLAG=""
#cd ${DIR_CONVERSION}
${CONV_BIN} -i $CRMC_OUT -o $CONV_OUT  -c 145 -b ${QGSJET2_FLAG} >& ${LOG_FILE}

####### DOUBLEARM #######
INPUT_DIR="${WORKDIR}"
INPUT_FILENAME="${CONV_OUTFILE}"

ln -s ${INPUT_FILENAME} +primary

DOUBLEARM_DIR="${LHCF_SIM_BASE}/DoubleArm/src/" 
DOUBLEARM_BIN="sepicsLHCPCLinuxIFC64"
# copy files to the working directory
cp $DOUBLEARM_DIR/$DOUBLEARM_BIN ./
cp $DOUBLEARM_DIR/FirstInput* ./
cp $DOUBLEARM_DIR/param ./
cp $DOUBLEARM_DIR/dpmjet.* ./
cp $DOUBLEARM_DIR/configdarm* ./
cp $DOUBLEARM_DIR/sepicsfile_jobs ./
cp $DOUBLEARM_DIR/epicsfile ./

# replace the random seed 
sed -e "s/-123456/$RUN/g" sepicsfile_jobs > sepicsfile

./$DOUBLEARM_BIN < FirstInput_nom > $OUTPUT_DIR/$OUTPUT_FILENAME 

# gzip the output file
gzip $OUTPUT_DIR/$OUTPUT_FILENAME

# remove the directory 
cd ../
rm -rf $WORKDIR

date
