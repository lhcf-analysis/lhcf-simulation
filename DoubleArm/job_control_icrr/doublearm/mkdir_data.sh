#!/bin/bash 

# Please execute this script before you submit jobs 
# because log ouput director must be present before your job submission.

DATADIR="/disk/lhcf/user/simulation_Run3/Data/DoubleArm/v1/"

mkdir -p $DATADIR/EPOSLHC/log/
mkdir -p $DATADIR/QGSJET2_04/log/
#mkdir -p $DATADIR/SIBYLL2.3d/log/
#mkdir -p $DATADIR/DPMJET3_2019/log/

DATADIR="/disk/lhcf/user/simulation_Run3/Data/DoubleArm/v1_wPipe/"

mkdir -p $DATADIR/EPOSLHC/log/
#mkdir -p $DATADIR/hepmc/QGSJET2_04/log/
#mkdir -p $DATADIR/hepmc/SIBYLL2.3d/log/
#mkdir -p $DATADIR/hepmc/DPMJET3_2019/log/

DATADIR="/disk/lhcf/user/simulation_Run3/Data/DoubleArm/v1_mass/"

mkdir -p $DATADIR/EPOSLHC/log/
mkdir -p $DATADIR/QGSJET2_04/log/
#mkdir -p $DATADIR/SIBYLL2.3d/log/
#mkdir -p $DATADIR/DPMJET3_2019/log/
