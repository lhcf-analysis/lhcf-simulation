
# Intel Fortran Compiler composer_xe_2013_sp1 (XE 14.0 Update1)_
source /mnt/lhcfs2/data1/local/intel/composer_xe_2013_sp1.1.106/bin/ifortvars.sh intel64

# Python 2.7 (<-> default 2.4 of lhcfs2)
module load gcc-4.9.4/python-2.7_gcc-4.9.4

export COSMOSTOP=/mnt/lhcfs2/data1/menjo/Simulation_Run3/Cosmos/Cosmos8.042_intel/
export COSMOSINC=${COSMOSTOP}/cosmos/
export EPICSTOP=/mnt/lhcfs2/data1/menjo/Simulation_Run3/Epics/Epics9.311_intel/
export EPICSINC=$EPICSTOP/epics
export PATH=${EPICSTOP}/Scrpt/:${COSMOSTOP}/Scrpt/:$PATH

export LHCF_SIM_BASE=/mnt/lhcfnas3/data/Simulation/Op2022/src/lhcf-simulation/

source /mnt/lhcfs2/data1/local/root/root_v5.34.32/bin/thisroot.sh
