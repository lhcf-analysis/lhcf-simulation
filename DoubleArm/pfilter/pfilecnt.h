#ifndef __PFILECNT_H__
#define __PFILECNT_H__

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string.h>
#include <sstream>
using namespace std;

typedef struct Particle{
  int    pcode;
  int    psubcode;
  int    pcharge;
  double ke;        // kinetic energy [GeV]
  double p[3];      // position   [cm]
  double w[3];      // direction  
  int    user;      // number defined by user
};

class pfilecnt {
private:
  string   ss;
  ifstream fin;
  ofstream fout;
  char     infilename[256];
  char     outfilename[256];
  bool     checkstart;
  Particle p;

public:
  pfilecnt();
  ~pfilecnt(){;}
  
  struct Particle* getparticle(){return &p;}
  int setinputfile(char* file);
  int setoutputfile(char* file);
  char* getinputfile(){return infilename;}
  char* getoutputfile(){return outfilename;}
  int inputfile(char* file="");
  int outputfile(char* file="");
  int readparticle();
  int writeheader();
  int writeendevent();
  int writeparticle(int iformat=0);
};

pfilecnt::pfilecnt(){
  setinputfile("test.out");
  setoutputfile("+primary");
  checkstart=true;
}

int pfilecnt::setinputfile(char* name){
  strcpy(infilename,name);
  return 0;
}

int pfilecnt::setoutputfile(char* name){
  strcpy(outfilename,name);
  return 0;
}

int pfilecnt::inputfile(char* file){
  if(strcmp(file,"")!=0){
    strcpy(infilename,file);
  }

  fin.open(infilename);
  if(!fin){
    cerr << "Cannot open " << infilename << endl;
    return -1;
  }
  return 0;
}

int pfilecnt::outputfile(char* file){
  if(strcmp(file,"")!=0){
    strcpy(outfilename,file);
  }
  fout.open(outfilename);
  return 0;
}

int pfilecnt::readparticle(){
  if(fin.eof()) return -1;

  unsigned long pos;
  getline(fin,ss); 
  istringstream sin;
  sin.str(ss);
  if(fin.eof()) return -1;

  //if(ss[0]=='#'){checkstart=true;return 3;}	
  if(checkstart==false){return 3;}

  if(ss.length() < 10){
    p.pcode = 0;
    return  1;
  }
  else if(ss.length() < 75){
    p.pcode = 0;
    return 2;
  }
  else {
    pos = ss.find("****");
    if(pos!=string::npos){
      ss.replace(pos,4,"   0");
    }
    pos = ss.find("***");
    if(pos!=string::npos){
      ss.replace(pos,3,"  0");
    }
    sin.str(ss);
    sin >> p.pcode >> p.psubcode >> p.pcharge
	>> p.ke >> p.w[0] >> p.w[1] >> p.w[2]
	>> p.user;
    p.p[0] = 0.0001;
    p.p[1] = 0.0001;
    p.p[2] = 0.0001;
    return 0;
  }
}

int pfilecnt::writeheader(){
  fout << "#  mul sub KE xyz dir  user disk49" << endl
       << "#--------------------------------" << endl;
  return 0;
}

int pfilecnt::writeendevent(){
  fout << endl;
  return 0;
}

int pfilecnt::writeparticle(int iformat){
  fout << setw(3) << p.pcode << " "
       << setw(2) << p.psubcode << " "
       << setw(2) << p.pcharge << "    ";
  fout.setf(ios::showpoint);
  fout.setf(ios::left);
  fout << setw(9) << setprecision (5) << p.ke << "  ";  
  fout.unsetf(ios::left);
  fout.setf(ios::fixed);
  if(iformat==0){
    fout << setw(7) << setprecision(4) << p.p[0] << " " 
	 << setw(7) << setprecision(4) << p.p[1] << " " 
	 << setw(7) << setprecision(4) << p.p[2] << " ";
  }
  else if(iformat==1){
    fout << setw(9) << setprecision(1) << p.p[0] << " " 
	 << setw(9) << setprecision(1) << p.p[1] << " " 
	 << setw(9) << setprecision(1) << p.p[2] << " ";
  }
  fout << setw(16) << setprecision(13) << p.w[0] << " "
       << setw(16) << setprecision(13) << p.w[1] << " "
       << setw(16) << setprecision(13) << p.w[2] << " ";
  fout.unsetf(ios::fixed);
  fout.unsetf(ios::showpoint);
  fout << setw(6) << p.user ;
  fout << endl;
  
  return 0;
}

#endif
