#ifndef __BEAMORBIT_H__
#define __BEAMORBIT_H__

#include <iostream>
#include <iomanip>
#include <cmath>
using namespace std;

// This function provide the beam orbit comming Arm#1 
// with zero crossing angle.
int beamorbit(double z,double &x, double &theta){
  // z     : input   [m] distance from I.P.
  // x     : output  [m] shift from beam pipe center;
  // theta : output  [rad] beam angle 

  const double z0 = 59.56;           // [m] start position of D1
  const double mlength = 3.4;        // [m] magnetic length 
  const double gaplength = 0.816;    // [m] distance between D1 units
  const double mangle = 0.18667E-3;  // [rad] bending angle/unit 
  const double mradius = 18213.3;    // [m] bending radius
  
  double zf=0.;
  double zb=0.;
  double xf=0.;
  double xb=0.;
  
  x = 0.;
  theta = 0.;
  zf = fabs(z);
  zf -= z0;
  
  if(zf>0){
    for(int ib=0;ib<6;ib++){
      if((zf-mlength)<0){
	zb += zf;
	theta += mangle/mlength*zb; 
	break;
      }
      else{ 
	zf -= mlength;
	theta += mangle;
      }
      
      if((zf-gaplength)<0){
	xf += mangle*(1+ib)*zf;	
	break;
      }
      else{
	zf -= gaplength;
	xf += mangle*(1+ib)*gaplength;
      }
      
      if(ib+1==6){
	xf += mangle*6.*zf;
      }
    }
    
    xb = mradius*theta*theta/2.;
  }
  
  x = xf + xb;

  if(z>0){
    x = -1.*x;
    theta = -1.*theta;
  }
  
  return 1;
}


#endif 
