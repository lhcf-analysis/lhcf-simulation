#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <sstream>
#include <ctime>
#include <cstdlib>
#include <cmath>
#include "pfilecnt.h"
#include "beamorbit.h"
using namespace std;

double rand_zposition(double zmin,double zmax);

int main(int argc, char** argv){
  srand(time(NULL));
  
  int maxnevent=-1;
  int startevent=0;
  double cutwz=0; 
  double zmin=-13979.,zmax=13979.;
  pfilecnt con;
  con.setinputfile("gencol.out");
  con.setoutputfile("+primary");

  for(int i=0;i<argc;i++){
    if(strcmp(argv[i],"-i")==0){
      con.setinputfile(argv[++i]);
    }
    if(strcmp(argv[i],"-o")==0){
      con.setoutputfile(argv[++i]);
    }
    if(strcmp(argv[i],"-nev")==0){
      maxnevent = atoi(argv[++i]);
    }
    if(strcmp(argv[i],"-sev")==0){
      startevent = atoi(argv[++i]);
    }
    if(strcmp(argv[i],"-wz")==0){
      cutwz = atof(argv[++i]);
    }
    if(strcmp(argv[i],"-zmin")==0){
      zmin = atof(argv[++i]);
    } 
    if(strcmp(argv[i],"-zmax")==0){
      zmax = atof(argv[++i]);
    } 
    if(strcmp(argv[i],"-h")==0 || 
       strcmp(argv[i],"-help")==0 || 
       strcmp(argv[i],"--help")==0){
      cout << "Primary Filter for Beam-Gas collision events---------------- " << endl
	   << "exp) pfilter -if gencol.out -of +primary -nev 100 -sev 10 -zmin -13979. -zmax 13979."<< endl;
      cout << "    -i  \"file\"  : input filename " << endl
	   << "    -o  \"file\"  : output filename " << endl
	   << "    -nev \"value\": number of converted events " << endl
	   << "    -sev \"value\": convertion start event number " << endl;
      cout << "    -zmin \"value\" : [cm] minimum z of collision points [default:-13979.]" << endl
	   << "    -zmax \"value\" : [cm] maximum z of collision points [default:13979.]" << endl;  
      return 0;
    }
  }

  if( con.inputfile("") < 0){  exit(-1);}
  con.outputfile("");
  con.writeheader();
  
  int ret;
  int nevent=0;
  int ievent=0;
  double x,y,z,theta,wx,wy,wz;
  // Assign the collision point for first event
  z = rand_zposition(zmin/100.,zmax/100.);
  beamorbit(z,x,theta);
  y = 0.;
  while(1){
    ret = con.readparticle();
    if(ret<0) {break;}
    else if(ret>=2){ continue; } 
    else if(ret==1){
      if(ievent>=startevent){
	con.writeendevent();
	nevent++;
	if(maxnevent>0 && nevent>=maxnevent){
	  break;
	}
	
	// Assign the collision point for next event.
	z = rand_zposition(zmin/100.,zmax/100.);
	beamorbit(z,x,theta);
	y = 0.;
      }
      ievent++;
      continue;
    }
    else if(ret==0){
      if(ievent>=startevent){
	// cuts
	// if( fabs(con.getparticle()->w[2]) < cutwz) { continue ;}
	
	con.getparticle()->p[0] = x*100.;   // m -> cm
	con.getparticle()->p[1] = y*100.;   // 
	con.getparticle()->p[2] = z*100.;   // 
       
	wx = con.getparticle()->w[0];
	wy = con.getparticle()->w[1];
	wz = con.getparticle()->w[2];
	con.getparticle()->w[0] = wx*cos(theta) + wz*sin(theta); 
	con.getparticle()->w[1] = wy;
	con.getparticle()->w[2] = -1.*wx*sin(theta) + wz*cos(theta); 
	
	con.writeparticle(1);
      }
    }
  }
  
  cout << "==== Primary Filter for DoubleArm ==============" << endl
       << "Input:  " << con.getinputfile() << endl
       << "Output: " << con.getoutputfile() << endl
       << "Nevent: " << nevent << endl
       << "Comments: " << endl;

  return 0;
}

double rand_zposition(double zmin,double zmax){
  return zmin + (zmax-zmin)*rand()/RAND_MAX;
}
