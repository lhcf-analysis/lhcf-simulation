      subroutine epUI( info, loc1, loc2)
! Basic purpose of this routine is do something when a particle makes
! an interaction. 
      implicit none
#include "ZepTrackv.h"
#include "Zcode.h"
      integer,intent(inout):: info  !  Basically, the particle code
                ! of the particle which made an interaction. For rare
                ! particles, they may be given a some unique value
                ! diff. from acutal code (and branch is made to
                ! "case default" )
                ! Exact code may be obtained by seeing Move.track.p.code.  

                ! This routine is called whennever
                ! a particle interacts, i.e, just after the interaction.
                ! The interaction products  are stacked in the particle
                ! stacking area.  The user can access to the stack
                ! by calling a subroutine with a stack index.
                ! The product is stacked with index from loc1 to loc2.
                ! In the case of  brems, 
                ! a gamma and the scattered  electron are the products.
                ! 
                ! When this is called, info is !=0.
                ! If the user dose not give any value on return, 
                ! this routine will not be called ***for that particle***
                ! again, until the next event  simulation starts. 
                ! The default is default to the letter.  If you require
                ! this routine be called for that particle  again within 
                ! the current event simulation, make info = 0 on return.
                !
      integer,intent(in)::loc1,loc2   ! see above
!                    To get information of a stacked track, use
!              call  epgetTrack(i, aTrack, icon) 
!          where  i must be loc1<= i <= loc2.  
!                 record /epTrack/ aTrack is the ouput
!                 integer icon == 0 ==> i is valid
!                         icon != 0 ==> i is invalid.
!

!  The user can access following variables for the interaction point.

!   Move.proc: the interaction type. see each "case" below
!          Cn:  component number  
!   Move.track:  interacting particle info. e.g  Move.track.pos.x etc
!          is the  position info. in local coordinate of Cn.
!          to convert it to world  coordinate,
!          call epl2w(Cn, Move.track.pos, posw)  for position
!          call epl2wd(Cn, dir,  dirtemp)        for direction cos

!       Move.track.p.code : code  
!       Move.track.p.fm.p(1:4): 4 momentum        

!   Media(MediaNo): media info. E.g  Media(MediaNo).name is the media name
!              (see Epics/epics/Zmedia.h) 
!   Media(MediaNo).colA (also colZ, colXs): target of nuclear interaction.
!                (A,Z) and cross-section (in mb)
!   a(i).p.code etc can be used to know produced particle properties.
!          (i=1,n).
!
      type(epTrack)::  aTrack
      integer icon, i, j, n, tag
      type(epPos):: posw

!      write(0,*) ' called with info=',info, loc1,loc2, Move.proc
!      write(0,*) 'Cn, code sub charge=',Cn, Move.track.p.code,
!     *  Move.track.p.subcode,Move.track.p.charge
!      write(0,*) 'Energy=',Move.track.p.fm.p(4)

      

      if( Move.proc == 'decay' ) then
         
         call epl2w(Cn, Move.track.pos, posw)
!         write(*,*) '#D', Cn, Move.proc, Move.track.pos, posw

!     Write the parent particle information
         n = loc2 - loc1 + 1
         tag = Move.track.user
         write(*,'(A,I10,3i4,4f13.6,2f10.5,f13.3,i4)')
     *        '#D_P ',
     *        tag,
     *        Move.track.p.code,
     *        Move.track.p.subcode,
     *        Move.track.p.charge,
     *        Move.track.p.fm.p(4),
     *        Move.track.p.fm.p(1),
     *        Move.track.p.fm.p(2),
     *        Move.track.p.fm.p(3),
     *        posw.x,
     *        posw.y,
     *        posw.z,
     *        n

!     Write the daugthers
         j = 1
         do i = loc1, loc2      ! print stacked products
            call epgetTrack(i, aTrack, icon)
            write(*,'(A,I10,3i4,4f13.6)')
     *           '#D_D ',
     *           j,
     *           aTrack.p.code,
     *           aTrack.p.subcode,
     *           aTrack.p.charge,
     *           aTrack.p.fm.p(4),
     *           aTrack.p.fm.p(1),
     *           aTrack.p.fm.p(2),
     *           aTrack.p.fm.p(3)
            j = j + 1
         enddo
         
         info = 0 
      endif

c$$$      select case(info)
c$$$!             you may freely modify "case" ; 
c$$$!             say, case(kpion:kgnuc) instead of case(kpion:kkaon)...
c$$$!             etc below
c$$$         case(kphoton)  ! gamma interaction
c$$$            ! do something for gamma ineraction
c$$$            ! if you want this routine be called again for this
c$$$            ! event,  make info =0 else don't touch it. Then,
c$$$            ! this routine will not be called until next event
c$$$            ! simulation starts.  
c$$$            !  Possilbe Move.proc values are
c$$$            !     comp : Compton scattering
c$$$            !     pair : pair creation
c$$$            !     phot : photoelectric effect
c$$$            !     coh  : coherent scattering
c$$$            !     photop : photo-production of hadrons
c$$$            !     mpair : magnetic pair creation
c$$$
c$$$         case(kelec)    ! electron interaction
c$$$             !   Move.proc
c$$$             ! brem : Bremstrahlung   
c$$$             ! knoc : knockon (Moller or Bhabka)
c$$$             ! anih : positron annihilation
c$$$             ! sync : synchroton emission
c$$$             ! Note-- Cerenkov light emission happens along the 
c$$$                     ! path of a charged particle and dose not
c$$$                     ! call this routine
c$$$         case(kmuon)     ! muon interaction
c$$$              ! knoc: knock-on
c$$$              ! decay:   decay   may include negative muon capture
c$$$              ! pair
c$$$              ! brem
c$$$              ! nuci : nuclear interaction
c$$$         case(kpion:kkaon)  ! pi, K
c$$$              ! knoc
c$$$              ! decay
c$$$              ! coll   collision
c$$$             
c$$$         case(knuc)       ! p,n
c$$$              ! knoc
c$$$              ! coll :  anti-prooton /anti-neutron annihilation  included
c$$$              ! dpair direct pair
c$$$
c$$$              ! To wait until nuclear collision takes place
c$$$!            if( Move.proc /= 'coll' ) then
c$$$!               info = 0
c$$$!            else
c$$$!               do i = loc1, loc2   ! print stacked products
c$$$!                  call epgetTrack(i, aTrack, icon) 
c$$$!                  write(0,*) i, aTrack.p.code
c$$$!              enddo
c$$$!            endif
c$$$
c$$$         case(kgnuc)     ! heavy int
c$$$              ! knoc
c$$$              ! coll :
c$$$              ! dpair  
c$$$            if( Move%proc == "dpair" .or.  Move%proc == "knoc" ) then
c$$$!               write(0,'(a, a, 2i4)')
c$$$!     *        ' proc loc1, loc2: ', trim(Move%proc), loc1, loc2
c$$$!               do i = loc1, loc2
c$$$!                  call  epgetTrack(i, aTrack, icon)
c$$$!                  if(aTrack%p%code == kelec) then
c$$$!                     write(0,'(a, 3i4, 1p, g14.4)')
c$$$!     *                     ' code sub Z  E=', aTrack%p%code,
c$$$!     *                   aTrack%p%subcode, aTrack%p%charge,
c$$$!     *                   aTrack%p%fm%p(4)
c$$$!                  endif
c$$$!               enddo
c$$$            endif
c$$$            info = 0
c$$$!         case(klight)  !  light interaction.   This is probably
c$$$                        !  nonsense for the moment
c$$$            
c$$$         case default   ! others.  mainly eta decay
c$$$              ! knoc, coll, decay.    
c$$$      end select
      end

      subroutine epGUI(info, sinfo)
!           This is for future extension. 
      use modGUI
      implicit none
!  #include "ZepTrackv.h"
!  #include "Zcode.h"
      integer,intent(in):: info
      type(gui),intent(inout)::sinfo
      end
