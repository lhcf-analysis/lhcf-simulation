      subroutine putorbit(co_z,co_x,co_y,theta)
      implicit none
      
      integer i
      real*8 co_z,co_x,co_y,theta
      real*8 tmp_z,tmp_x,tmp_theta 
      
      if( abs(co_z) .lt. 59.302d2) then 
         co_x = 0.
         co_y = 0.
         theta = 0.
      else if( abs(co_z) .lt. 59.302d2 + 4.266d2*6 )  then 
         tmp_z = abs(co_z) - 59.302d2
         tmp_x = 0.
         do i=1,6
            if(tmp_z .lt. 0.514d2) then
               tmp_x = tmp_x + (i-1.)*0.00018667*tmp_z
               exit
            else 
               tmp_x = tmp_x + (i-1.)*0.00018667*0.514d2
            endif
            if(tmp_z .lt. 3.914d2) then
               tmp_theta = tmp_theta + 0.00018667/3.4d2*(tmp_z-0.514d2)
               tmp_x = tmp_x 
     *              + 18213.3d2*(cos((i-1.)*0.00018667)-cos(tmp_theta))
               exit
            else 
               tmp_theta = tmp_theta + 0.00018667
               tmp_x = tmp_x 
     *              + 18213.3d2*(cos((i-1.)*0.00018667)-cos(tmp_theta))
            endif
            if(tmp_z .lt. 4.266d2) then
               tmp_x = tmp_x + i*0.00018667*(tmp_z - 3.914d2)
               exit
            else  
               tmp_x = tmp_x + i*0.00018667*(4.266d2 - 3.914d2)
            endif
            tmp_z = tmp_z - 4.266d2
         end do

      else if ( abs(co_z) .lt. 139.8d2) then
         tmp_z = abs(co_z)
         tmp_x = 1.4412 +  6*0.00018667*(tmp_z-(59.302d2 + 4.266d2*6))
         tmp_theta = 6*0.00018667
      endif

      if(co_z .gt. 0.) then 
         co_x = -1.*tmp_x
         co_y = 0.
         theta = 1.*tmp_theta
      else 
         co_x = 1.*tmp_x
         co_y = 0.
         theta = 1.*tmp_theta
      endif

      RETURN

      end
      
