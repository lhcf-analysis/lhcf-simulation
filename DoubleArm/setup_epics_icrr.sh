
module load intel/2022.3.1

export COSMOSTOP=/disk/lhcf/user/simulation_Run3/COSMOS/Cosmos8.042/
export COSMOSINC=${COSMOSTOP}/cosmos/
export EPICSTOP=/disk/lhcf/user/simulation_Run3/EPICS/Epics9.311/
export EPICSINC=$EPICSTOP/epics
export PATH=${EPICSTOP}/Scrpt/:${COSMOSTOP}/Scrpt/:$PATH

export LHCF_SIM_BASE=/disk/lhcf/user/simulation_Run3/lhcf-simulation/

source /disk/lhcf/user/simulation_Run3/local/root6/bin/thisroot.sh
