# DoubleArm 

DoubleArm simulates the transportation of particles from the interaction point upto TAN (139.8 m from IP1). This writes the information of particles reaching to TAN (10 cm radius).

## Setup DoubleArm

0. Select the magnetic field of Q1 magnets.
The magnetic field strength must be set the symbolic link to the values for the beam energy which you want to use. For example, for sqrt{s} = 13.6 TeV,
```
rm eppos2B.f
ln -s eppos2B.f.6.8TeV_wQ1 eppos2B.f 
```
The files with _woQ1 are without Q1 magnetic field, which should be used only for test purposes.

1. Compile  
Before make, please set the environmental valuables (COSMOSTOP, EPICSTOP and etc.) properly. Before compile, you must configure the EPICS to use the "ecyl" shape (see  [install_EPICS.md](install_EPICS.md)) (you need to do it only once).  
```
make 
```

2. Prepare dpmjet.inp and .GLB  
These files are parameters files for DPMJET III. If you use Epics9.311, you do not need this step. Only in case that you want to simulate with another version of EPICS, you need to re-generate these files. 
```
fordpmjet configdummy_fordpmjet
```

## How to use

1. configure your simulation  
   1. sepicsfile  
   A user must modify this file for your calculation. Important parameters are following;
       - Ir1(1) Ir1(2) : Random number seed. If it is negative, the seed is automatically set from CPU time and PID at the execution.
       - Nevent : Number of events. If you find two numbers in a line, only the second value is used. 
       - PrimaryFile : Input file for the events (=CRMC/conversion output). The first character of the filename must be "+" to use the format of CRMC/conversion output. 
   1. epicsfile  
   This file specifies many parameters for Epics calculation like model for electromagnetic interaction.  
   ** DO NOT CHANGE THIS FILE **  
   *Note)*  In case of Epics9.165 (used old MC for op2015 and before.), you need to switch the symbolic link to epicsfile_epics9.165. The required parameter format is different between the versions. Additionally you need to specify the path of Epics/Data/Media directory manually. *$EPICSTOP* does not work in this version of EPICS. 
   1. FirstInput 
   The list of the parameter files, which should be given to the application. In this file, the geometrical configuration file is specified. 
       - configdarm_nom (default) : configuration without beam pipe material (=material are replaced copper to Air). 
       - configdarm_m : with beam pipe material. This can generate the background particles due to beam pipe and secondaries interactions. It is precise but the calculation is quite slow. The energies of these background particles are below 100 GeV. 

2. run the simulation  
The simulation result is printed in the standard output. So you must use redirect due to save your results to the file. 

```
./sepicsLHCPCLinuxIFC64 < FirstInput > out.txt
```

## Format
### input format (output of CRMC+convert)

```
#  mul sub KE xyz dir  user disk49
#--------------------------------
  4 -1  1 3.641261406e+02      0.0001  0.0001  0.0001  -0.000382218720082 -0.000716406837341  0.999999670334992              1
  4  1 -1 8.995348438e+01      0.0001  0.0001  0.0001  -0.000210636754299 -0.000760526490330 -0.999999688615759              2
  4 -1  1 4.350039450e+02      0.0001  0.0001  0.0001  -0.000812798363247 -0.000563232455973  0.999999511063891              3
  6 -1  0 7.187585531e+02      0.0001  0.0001  0.0001   0.000001296345466 -0.000920984723922 -0.999999575892639              4
  4 -1  1 9.071382299e+02      0.0001  0.0001  0.0001  -0.000665142693937 -0.000041980060653  0.999999777911411              5
  6 -1  1 1.250392124e+03      0.0001  0.0001  0.0001  -0.000106064770659 -0.000378287157101 -0.999999922824543              6
  4  1 -1 5.254922368e+02      0.0001  0.0001  0.0001  -0.000631039902980  0.000343070861120 -0.999999742045479              7
  1  0  0 8.439774036e+01      0.0001  0.0001  0.0001  -0.000805158308538 -0.000084108967339  0.999999672322836              8
  1  0  0 3.599484897e+02      0.0001  0.0001  0.0001   0.000220254785181  0.000344357700113  0.999999916452799              9
  1  0  0 2.703412812e+02      0.0001  0.0001  0.0001   0.000334877431940  0.000748397611114 -0.999999663879004             10

  4 -1  1 7.839045727e+02      0.0001  0.0001  0.0001   0.000276661072308 -0.000629053413690 -0.999999763875199            101
  4  1 -1 1.375617332e+02      0.0001  0.0001  0.0001   0.000655714372206 -0.000093335284915  0.999999780663569            102
  4 -1  1 4.276999609e+02      0.0001  0.0001  0.0001  -0.000588821468401  0.000606311127398  0.999999642837984            103
  4  1 -1 4.956334865e+02      0.0001  0.0001  0.0001   0.000739866359067 -0.000230630658069  0.999999699703590            104
  4 -1  1 6.607098535e+02      0.0001  0.0001  0.0001  -0.000359663415875  0.000054753524154  0.999999933822137            105
  1  0  0 1.279820360e+03      0.0001  0.0001  0.0001   0.000218476961043 -0.000057950763490  0.999999974454763            106
  1  0  0 1.710561459e+02      0.0001  0.0001  0.0001  -0.000900738789336 -0.000401993190511  0.999999513535436            107
  4  1 -1 7.765008914e+02      0.0001  0.0001  0.0001   0.000123489407102  0.000054882765383 -0.999999990869124            108
  1  0  0 1.805007998e+02      0.0001  0.0001  0.0001   0.000153059426474  0.000217177291069 -0.999999964703417            109
  1  0  0 2.058513722e+02      0.0001  0.0001  0.0001  -0.000309357477448 -0.000096620807485 -0.999999947481184            110
  1  0  0 2.228486758e+02      0.0001  0.0001  0.0001   0.000210236975009  0.000259988560190 -0.999999944103180            111
  1  0  0 2.007740791e+02      0.0001  0.0001  0.0001   0.000100917505551  0.000870000911408 -0.999999616456962            112
```

* The first three integer : particle code, sub-code and charge. 
* Next : Kinetic energy of the particle 
* 0.0001 0.0001 0.0001 : the injection position 
* Next three : direction (unit vector)
* The last: User defined valuable (here using EventNumber * 100 + i_secondaries )
* The blank lines mean the separation between the events. 

### output format 
```
#IN           1   6  -1   1  3875.956692    -0.360036     0.451760  3875.956536   0.00010   0.00010      0.00010
#IN           2   4  -1   1   753.234233    -0.183985     0.104900  -753.234190   0.00010   0.00010      0.00010
#IN           3   4   1  -1  1394.192024    -0.075091    -0.054149 -1394.192014   0.00010   0.00010      0.00010
#IN           4   4   0   0  2127.101084     0.154663    -0.234101 -2127.101061   0.00010   0.00010      0.00010
#IN           5   6  -1   1  2137.855985     0.553795     0.097983 -2137.855705   0.00010   0.00010      0.00010
#IN           6   4  -1   1   317.530429    -0.133294    -0.065599  -317.530363   0.00010   0.00010      0.00010
#D_P          4   4   0   0  2127.101084     0.154663    -0.234101 -2127.101061   0.00010   0.00009     -0.04628   2
#D_D          1   1   2   0  1117.143162     0.148475    -0.127435 -1117.143145
#D_D          2   1   2   0  1009.957922     0.006188    -0.106666 -1009.957916
     1           4  1  2  0   1009.957922   1.47888   0.08576  -1.47639  -13980.050       0.000  0.000006127 -0.000105614 -0.999999994
     1           4  1  2  0   1117.143162   2.44857   1.85813  -1.59463  -13980.050       0.000  0.000132906 -0.000114072 -0.999999985

#IN         101   6  -1   1  3163.981356     0.148073     0.357847  3163.981193   0.00010   0.00010      0.00010
#IN         102   6  -1   0  2170.317365     0.327870     0.091141 -2170.317135   0.00010   0.00010      0.00010
#IN         103   6   1   0   267.920229     0.220822    -0.061832  -267.918483   0.00010   0.00010      0.00010
#IN         104   4  -1   1  1406.443514    -0.159279     0.227946 -1406.443480   0.00010   0.00010      0.00010
#IN         105   4   1  -1   297.677801     0.023232    -0.143609  -297.677733   0.00010   0.00010      0.00010
#IN         106   4   0   0   374.308728     0.061416     0.139548  -374.308673   0.00010   0.00010      0.00010
#IN         107   4  -1   1  2385.462705    -0.373530    -0.354945  2385.462645   0.00010   0.00010      0.00010
#IN         108   4   0   0   244.139900     0.118569     0.084843   244.139819   0.00010   0.00010      0.00010
#IN         109   4   1  -1  1065.486768    -0.265820     0.264429 -1065.486692   0.00010   0.00010      0.00010
#D_P        108   4   0   0   244.139900     0.118569     0.084843   244.139819   0.00010   0.00010      0.00775   2
#D_D          1   1   2   0    22.391269    -0.001868     0.044592    22.391225
#D_D          2   1   2   0   221.748631     0.120437     0.040251   221.748594
#D_P        106   4   0   0   374.308728     0.061416     0.139548  -374.308673   0.00010   0.00010     -0.00115   2
#D_D          1   1   2   0   334.550934     0.048450     0.083644  -334.550920
#D_D          2   1   2   0    39.757794     0.012966     0.055905   -39.757753
     2         106  1  2  0    334.550934   4.03943   2.02471   3.49536  -13980.050       0.000  0.000144821  0.000250018 -0.999999958
     2         102  6 -1  0   2169.377799   2.19217   2.11207   0.58719  -13980.050       0.000  0.000151070  0.000041995 -0.999999988
 
#IN         201   6  -1   1   538.869639     0.350637    -0.048507   538.868706   0.00010   0.00010      0.00010
#IN         202   6  -1   1  1915.551191     0.435629    -0.246808 -1915.550896   0.00010   0.00010      0.00010
#IN         203   4  -1   1  1316.824597    -0.056846    -0.123387 -1316.824583   0.00010   0.00010      0.00010
#IN         204   4   0   0  1420.352459    -0.368447    -0.029399 -1420.352405   0.00010   0.00010      0.00010
#IN         205   6   1  -1  1208.797326    -0.214517     0.428235  1208.796867   0.00010   0.00010      0.00010
#IN         206   4   1  -1   273.135827     0.094186    -0.137451   273.135741   0.00010   0.00010      0.00010
#IN         207   4   1  -1   619.625619    -0.262314     0.011980  -619.625548   0.00010   0.00010      0.00010
#D_P        204   4   0   0  1420.352459    -0.368447    -0.029399 -1420.352405   0.00010   0.00010     -0.00435   2
#D_D          1   1   2   0  1324.895937    -0.375630    -0.016402 -1324.895883
#D_D          2   1   2   0    95.456523     0.007182    -0.012997   -95.456521
     3         204  1  2  0     95.456523   2.17474   1.05201  -1.90335  -13980.050       0.000  0.000075243 -0.000136155 -0.999999988
     3         204  1  2  0   1324.895937   3.96725  -3.96347  -0.17297  -13980.050       0.000 -0.000283516 -0.000012380 -0.999999960

```
All lines with starting '#' are for information. The others are for DoubleArm outputs. The empty lines are partitions among events. The format of the output is 
```
#ev  tag  code subcode charge ke r x y z wx wy wz
```
* #ev: Event Number
* tag: Event Tag (transferred from the input value). If a particle hits the beam pipe material, the sign is set to negative.
* code, subcode, charge: Particle code, sub-code and charge.
* ke : Kinetic Energy (GeV)
* r : distance from the beam pipe center (=not beam center if non-zero crossing angle)
* x,y,z : x, y and z position (cm)
* wx,wy,wz : direction of the particle (a unit vector)

**lines with "#IN", "#D_P" and "#D_D"**  
These lines correspond to information of the incident particles and the decayed particles during the transportation. #IN lines are for incident particles, #D_P and #D_D lines are for parents and daughters, respectively. The format are 
```
#IN     user_tag  code subcode charge energy px py pz incident_x y z
#D_P    user_tag  code subcode charge energy px py pz vertex_x y z number_of_daughters
#D_D          id  code subcode charge energy px py pz 
``` 

## Note
* **DO NOT USE** pfilter  
  pfilter was used for the MC generation for <= 2015, which applied the crossing angle (as rotation, not Lorentz boost) and filter input particles. But no more you should use it because CRMC/conversion treats them more precisely.
