
# Primaryfilter 
The output files of DoubleArm do not fit the input format of End2End. So primaryfilters convert the format to that of End2End input (format of +primary file). In addition, it also 

* **The beam center treatment:** The coordinate of End2End is alway on the beam pipe system (the beam pipe center and z=139.8m is the origin). In the data, the beam center position was found to be shifted from the ideal position. This difference of beam center position is treated as **an shift** of incident particle position. We had a discussion about this treatment, simple shift or rotation, and then we concluded that a simple shift is used because of negligible difference of results between the methods.  
* **Particle filtering:** With some options, you can cut some particles like hitting TAN wall (-wTANWCut). This selection is applied to each *particle* (not the event).
* **Event filtering (since version 3):** With --selection_anyhit option, you can select *events* which have any hit in any LHCf calorimeter (including 1 mm outside margin). This was introduced for Op2022 E2E configuration. Because of the ZDC presence, even if no hit in LHCf detector, huge computation time is required for the event. To avoid such CPU time for non-useful events, the option should be applied. 
* **Separation of a multi-hit events:** In multi-hit events like pi0 events or photon + neutron, sometimes it is useful to know the detector output of each particle not only the summation of them. With --mhseparation, such a multi-hit event is separated to multiple events with a single hit. It was introduced for Op2022, and this option should be always ON. The combine of the multiple events to the original single event can be treated in LHCfLibrary. See the example to know how to identify events from a single pp collision.
* **file separation:** It can separate a single file to multiple files with smaller numbers of events. Use --fileseparation option. 


## How to use 
```
 primaryfilter_v3 *************************************************
 ./bin/primaryfilter_v3 --Op2022_Arm1_Center -i [inputfile] -o [outputfile]

 Suggested option for Op2022 MC
 ./bin/primaryfilter_v3 --Op2022_Arm1_Center -i darm_run00001.out 
   -o e2eprimary_run00001_%03d.out --mhseparation --fileseparation 1000 
   --selection_anyhit

 Options:
 type sel:  --Op2022_Arm1_Center, --Op2022_Arm1_5mmHigh, 
            --Op2022_Arm2_Center, --Op2022_Arm2_5mmHigh
            --Op2015_Arm1_Center, --Op2015_Arm1_5mmHigh, 
            --Op2015_Arm2_Center, --Op2015_Arm2_5mmHigh
 -i      : input file name (output of DoubleArm)
 -o      : output file name ("+primary")
 -wBPCut : particle selection with ellipse beam-pipe
          : Use this cut for outputs of DoubleArm without matter.
 -wTANWCut: particle selection hitting in the tan wall cut (abs(x)<4.8cm)
 -wBKGCut:  particle selection with Backgound Cut.
 --inverse:  inverse input for the detector. (z_pos -> Arm2, z_nag -> Arm1)
 --mhseparation: generate event for individual incident particle. (default)
 --no_mhseparation: mhseparation option to off
 --fileseparation (limit): Save results to files separately for every (limit) events.
             in this case, please specify the output filename as output_%03d.root
 --selection_anyhit: Select events with any hit in LHCf calorimeters.
```

Please select the type. It set parameters automatically: the detector (Arm1 or Arm2), the detector vertical position (important for event filtering), and the beam center position. 

## Example 
Input file (output of DoubleArm)
```
     1           4  1  2  0 1790.4630       1.85973   0.68293  -1.72980  -13980.050       0.000  0.000048843 -0.000123741 -0.999999991
     1           4  1  2  0 336.63807       2.83949   2.79129  -0.52098  -13980.050       0.000  0.000199655 -0.000037273 -0.999999979

     2         108  1  2  0 144.81249       4.72029   4.57852  -1.14814   13980.050       0.000  0.000327497 -0.000082134  0.999999943
     2         106  1  2  0 251.32641       3.96314  -0.95679   3.84591  -13980.050       0.000 -0.000068447  0.000275092 -0.999999960
     2         102  6 -1  0 2169.3778       2.19217   2.11207   0.58719  -13980.050       0.000  0.000151070  0.000041995 -0.999999988

     3         204  1  2  0 954.37822       2.97109  -2.85838  -0.81055  -13980.050       0.000 -0.000204469 -0.000057986 -0.999999977
     3         204  1  2  0 465.97424       5.25735  -5.19940   0.77840  -13980.050       0.000 -0.000371924  0.000055673 -0.999999929

     4         309  1  2  0 776.32650       4.21361   2.36957   3.48420   13980.050       0.000  0.000169489  0.000249219  0.999999955
     4         301 18 -1  0 1655.4986       1.65045  -0.63376  -1.52393   13980.050       0.000 -0.000045340 -0.000109014  0.999999993

     5         401  6 -1  1 6799.0579       7.69949  -7.69939   0.03926   13980.050       0.000 -0.001123746 -0.000000507  0.999999369

     6         507  1  2  0 134.47420       3.91081   3.86750  -0.58037  -13980.050       0.000  0.000276637 -0.000041521 -0.999999961
     6         505  5  5  0 1159.1058       2.87970  -0.90388   2.73417  -13980.050       0.000 -0.000064662  0.000195569 -0.999999979

     7         601  6 -1  1 6219.2698       8.41890  -8.41890   0.00278   13980.050       0.000 -0.001207074  0.000031606  0.999999271

     8         703  6 -1  0 1976.5796       0.58672   0.38110   0.44609   13980.050       0.000  0.000027253  0.000031902  0.999999999
     8         701  6 -1  1 6651.2014       7.83139  -7.83090  -0.08759  -13980.050       0.000 -0.001154630  0.000003990 -0.999999333
```

Output file converted with the options of --mhseparation and --selection_anyhit
```
# mul sub KE xyz dir  user disk49
#---------------------------------
18 -1  0    1655.4986  -0.63376  -1.52393   0.00000 -0.000045340 -0.000109014  0.999999993       301.01

 1  2  0     776.3265   2.36957   3.48420   0.00000  0.000169489  0.000249219  0.999999955       309.02

 6 -1  0    1976.5796   0.38110   0.44609   0.00000  0.000027253  0.000031902  0.999999999       703.01

 1  2  0    1730.7705  -0.76563   3.66854   0.00000 -0.000054773  0.000262405  0.999999964      1704.01

 6 -1  0    3121.1897   0.41247   1.77688   0.00000  0.000029497  0.000127094  0.999999991      2304.01

 1  2  0    2400.2929   0.01306  -1.92094   0.00000  0.000000927 -0.000137413  0.999999991      2507.01

 1  2  0    77.841613   0.58178   2.40689   0.00000  0.000041608  0.000172160  0.999999984      2507.02

 1  2  0    547.85042  -0.00062   4.16760   0.00000 -0.000000051  0.000298103  0.999999956      2709.01

 1  2  0    331.94894  -4.40121   3.70866   0.00000 -0.000314828  0.000265275  0.999999915      2709.02
```

* First three integers : particle code, subcode, and charge
* 4th value : Kinetic Energy
* 5-7th values : x,y,z position (beam center shift consider for x,y, z position is always zero, which is at 139.8 m from IP.)
* 8-10th values : direction (unit vector)
* 11th value : user tag  

**User tag format:** The integer value of user tag is the value transferred from DoubleArm (= two lowest digit for parent particle number in the event of CRMC, the other digits for event ID number.). The decimal value is added in this primaryfilter and it corresponds to the particle number on End2End input. For example, only one K0s generates in XXth events of CRMC and decay to four photons in DoubleArm. In this case, the user tag for four events might be XX01.01, XX01.02, XX01.03 and XX01.04. 