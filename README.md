# LHCf Simulation

## Requirements 
- CRMC Version 1.8.0
  - HepMC2 
  - Boost 1.8.2
- COSMOS/EPICS
  - COSMOS Version 8.042
  - EPICS Version 9.311
  - Intel Fortran Compiler

Note)  
- These required libraries/application and versions are since Op 2022 MC. Before that, Gencol (a tool in Epics) was used for pp event generation in stead of CRMC, and older version of COSMOS/EPICS was used (Cosmos7.645/Epics9.165).
- Newest version of CRMC is 2.0.1. It requires HepMC3 and it could not be installed in ICRR system. In the future, it may need to be replaced.

## MC procedures

There are three main steps and two sub-step in this simulation from pp or pO collision to the detector simulation.

1. [CRMC](./doc/crmc.md) : Event generation of collisions like pp at $\sqrt{s}$ = 13.6 TeV
1. [HepMCtoGencol](./doc/crmc.md#conversion-from-hepmc-format-to-gencol-format-input-of-doublearm) : Format conversion from the CRMC format (hepmc.gz) to the DoubleArm input (text file). The crossing angle is also considered in this conversion.
1. [DoubleArm](./doc/DoubleArm.md) : Transportation from IP to the front of TAN (+/-139.8m from IP).
1. [PrimaryFilter](./doc/PrimaryFilter.md) : Format conversion from DoubleArm output to End2End input. Several selections are applied in this code; Selection Arm1 side or Arm2 side, Beam center shift, multi-hit separation etc.
1. [End2End](./doc/End2End.md) : Detector simulation with Arm1 and Arm2 including TAN, Y-chamber structures and ATLAS ZDC (since Op 2022).

## Installation and setup

1. [Install EPICS/COSMOS](./doc/install_EPICS.md) to your system.  
The COSMOS/EPICS is already available in Nagoya/ICRR/CNAF clusters.
To setup, please load a configuration file (DoubleArm/setup_epics_****.sh) 
1. Install CRMC (Optional). As an example of installation, [setuplog_ICRR](./doc/SetupLog_ICRR.txt)
1. Compile the codes. Execute ```make``` in each source directory like DoubleArm/src/  
For DoubleArm/End2End, some setups are needed to configure it. See [DoubleArm](./doc/DoubleArm.md) and [End2End](./doc/End2End.md)

## Mass production for Op2022

### Strategy and rule

- **CRMC and DoubleArm**: This CPU time is negligible respect to End2End CPU time. So all CRMC and DoubleArm calculation for official MC samples will be done in ICRR clusters to have consistent configuration for all events. 
- **Run and Sub-run**: A run corresponds to a single execution of CRMC and its number of events should be 10^5 collisions in each run (roughly 3 hours calculation by a single CPU). A sub-run corresponds to a single execution of End2End and its number of events should be 10^3 collisions in each sub-runs. So one run includes 100 sub-runs. The separation from a single run file to multiple sub-run files is done by PrimaryFilter. These sub-run files can be combined to a single file in LHCfLibrary/MCtoLevel0.
- **Random Seed**: Each run and sub-run should have a unique number using these run and sub-run numbers. For example, one of the random seed parameters of DoubleArm, Ir1(2) should be 101 for Run 101.
- **Primaryfilter**: The multihit separation option of Primaryfilter must be "ON" always. It helps us to do some analyses of multi-hit event effect. The event selection option (--selection-anyhit) must be "ON" also to reduce the computation time.
- **End2End**: Because of multihit seperation in PrimaryFilter, the event number of E2E output does not corresponds to the collision number in CRMS anymore. So it is not simple to know how many collisions have been computed (because a E2E job crashes sometime during the calculation). To estimate it, it is better to complete all jobs successfully (it means if a job crashed, it is better to re-submit the job.). 
- **file name**: The each file name should include a few configuration information (model, run number, detector).
  - CRMC output: crmc_EPOSLHC_000001.hepmc.gz
  - DoubleArm output: darm_EPOSLHC_000001.out.gz
  - End2End output: e2e_a1c_EPOSLHC_000001_001.out

### Script for job submission
The job submission and control script for clusters are available in XXXX/job_control_*/ directories.
-  CRMC + HepMCtoGencol:  CRMC/job_control_icrr/crmc/loop.py  
-  DoubleArm: DoubleArm/job_control_icrr/doublearm/loop.py
-  End2End: End2End/job_control_japan/end2end/loop.py

PrimaryFilter computation finish quickly. So a single CPU is enough to do it even for many files. A script to apply PrimaryFilter to all files in a given directory path is also available; End2End/job_control_japan/primaryfilter/generate_primarys.py


### Production status 

## Versions 

- 15 April 2024 : Menjo updated DoubleArm to dump the decay information of particles.




